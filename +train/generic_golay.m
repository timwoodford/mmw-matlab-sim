function [GaSeq,GbSeq] = generic_golay(Wk, Dk, N)
%TRAIN_GENERIC_GOLAY Generate Golay sequences from Wk vector
%   Returns complementary Golay sequences - mostly copied from edaboard
%   forum entry

% From the original:
%    File Name:     GolayCorrelator.m
%    Authors:       Prem Kiran Nerella
%    Version:       1.0
%    History:       May 2014 created

import train.ring_buffer;

CodeLen=N;
B0_rb = ring_buffer([], [], Dk(1), 'init');
B1_rb = ring_buffer([], [], Dk(2), 'init');
B2_rb = ring_buffer([], [], Dk(3), 'init');
B3_rb = ring_buffer([], [], Dk(4), 'init');
B4_rb = ring_buffer([], [], Dk(5), 'init');
if(CodeLen > 32)
    B5_rb = ring_buffer([], [], Dk(6), 'init');
end
if(CodeLen > 64)
    B6_rb = ring_buffer([], [], Dk(7), 'init');
end
%Correlator Buffer Flag 'Bx_f' indicate whether Buffer (elements) Count is equal to 'Dk(x)' 
B0_f = 0;
B1_f = 0;
B2_f = 0;
B3_f = 0;
B4_f = 0;
if(CodeLen > 32)
    B5_f = 0;
end
if(CodeLen > 64)
    B6_f = 0;
end

% This is how we do sequence generation
InpSeq=[1 zeros(1,CodeLen-1)];
%Input samples length
%Initialize output
GaSeq = zeros(1,N);
GbSeq = zeros(1,N);

%Golay Correation 
% /*************************************************************************************
%Golay Sequences specified in IEEE 802.11ad Std are defined as described below

%A_[0] = 1;
%B_[0] = 1;

%A_[k](n) = Wk * A_[k-1](n) + B_[k-1](n-Dk);
%B_[k](n) = Wk * A_[k-1](n) - B_[k-1](n-Dk);

%Note that A_[k](n) , B[k](n) are zero for n < 0

%Ga_[128](n) = A_[7](128 - n)
%Gb_[128](n) = B_[7](128 - n)

%Ga_[64](n) = A_[6](64 - n)
%Gb_[64](n) = B_[6](64 - n)

%Ga_[32](n) = A_[5](32 - n)
%Gb_[32](n) = B_[5](32 - n)
%  *************************************************************************************/
for n = 1:N

    m = n-1;
    %A_[0] & B_[0]
    A_0 = InpSeq(n);B0 = A_0;
    if(B0_f == 0) if((m-Dk(1)) >= 0) [B0_rb B_0] = ring_buffer(B0_rb, [], [], 'pop'); B0_f = 1; else B_0 = 0; end; else [B0_rb B_0] = ring_buffer(B0_rb, [], [], 'pop'); end        
    A1 = Wk(1)*A_0 + B_0;
    B1 = Wk(1)*A_0 - B_0;
    %A_[1] & B_[1]
    A_1 = A1;
    if(B1_f == 0) if((m-Dk(2)) >= 0) [B1_rb B_1] = ring_buffer(B1_rb, [], [], 'pop'); B1_f = 1; else B_1 = 0; end; else [B1_rb B_1] = ring_buffer(B1_rb, [], [], 'pop'); end 
    A2 = Wk(2)*A_1 + B_1;
    B2 = Wk(2)*A_1 - B_1;
    %A_[2] & B_[2]
    A_2 = A2;
    if(B2_f == 0) if((m-Dk(3)) >= 0) [B2_rb B_2] = ring_buffer(B2_rb, [], [], 'pop'); B2_f = 1; else B_2 = 0; end; else [B2_rb B_2] = ring_buffer(B2_rb, [], [], 'pop'); end
    A3 = Wk(3)*A_2 + B_2;
    B3 = Wk(3)*A_2 - B_2;
    %A_[3] & B_[3]
    A_3 = A3;
    if(B3_f == 0) if((m-Dk(4)) >= 0) [B3_rb B_3] = ring_buffer(B3_rb, [], [], 'pop'); B3_f = 1; else B_3 = 0; end; else [B3_rb B_3] = ring_buffer(B3_rb, [], [], 'pop'); end
    A4 = Wk(4)*A_3 + B_3;
    B4 = Wk(4)*A_3 - B_3;
    %A_[4] & B_[4]
    A_4 = A4;
    if(B4_f == 0) if((m-Dk(5)) >= 0) [B4_rb B_4] = ring_buffer(B4_rb, [], [], 'pop'); B4_f = 1; else B_4 = 0; end; else [B4_rb B_4] = ring_buffer(B4_rb, [], [], 'pop'); end
    A5 = Wk(5)*A_4 + B_4;
    B5 = Wk(5)*A_4 - B_4;
    %A_[5] & B_[5]
    if(CodeLen > 32)
        A_5 = A5;
        if(B5_f == 0) if((m-Dk(6)) >= 0) [B5_rb B_5] = ring_buffer(B5_rb, [], [], 'pop'); B5_f = 1; else B_5 = 0; end; else [B5_rb B_5] = ring_buffer(B5_rb, [], [], 'pop'); end
        A6 = Wk(6)*A_5 + B_5;
        B6 = Wk(6)*A_5 - B_5;
    end
    %A_[6] & B_[6]
    if(CodeLen > 64)
        A_6 = A6;
        if(B6_f == 0) if((m-Dk(7)) >= 0) [B6_rb B_6] = ring_buffer(B6_rb, [], [], 'pop'); B6_f = 1; else B_6 = 0; end; else [B6_rb B_6] = ring_buffer(B6_rb, [], [], 'pop'); end
        A7 = Wk(7)*A_6 + B_6;
        B7 = Wk(7)*A_6 - B_6;
    end
    %A_[7] & B_[7]
    [B0_rb dataOut] = ring_buffer(B0_rb, B0, [], 'push');
    [B1_rb dataOut] = ring_buffer(B1_rb, B1, [], 'push');
    [B2_rb dataOut] = ring_buffer(B2_rb, B2, [], 'push');
    [B3_rb dataOut] = ring_buffer(B3_rb, B3, [], 'push');
    [B4_rb dataOut] = ring_buffer(B4_rb, B4, [], 'push');
    GaSeq(n) = A5;
    GbSeq(n) = B5;
    if(CodeLen > 32)
        [B5_rb dataOut] = ring_buffer(B5_rb, B5, [], 'push');
        GaSeq(n) = A6;
        GbSeq(n) = B6;
    end
    if(CodeLen > 64)
        [B6_rb dataOut] = ring_buffer(B6_rb, B6, [], 'push');
        GaSeq(n) = A7;
        GbSeq(n) = B7;
    end
end
%Normalize
% GaSeq = GaSeq/CodeLen;
% GbSeq = GbSeq/CodeLen;
end

