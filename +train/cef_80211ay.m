function sequence = cef_80211ay(n_spatial_streams,spatial_stream_i)
%TRAIN_80211AY_CEF Generate the 802.11ay draft 2016-08 chan-est field
%   Channel estimation in ay has two purposes: first, to bond multiple
%   frequency channels (which we don't need - channels are ~2 GHz each),
%   and second to do spatial channel estimation
% See 802.11-16/0994r1 from TGay

import train.*;

if n_spatial_streams > 8
    error('Limited to 8 maximum spatial streams');
end

if spatial_stream_i > n_spatial_streams
    error('Spatial stream index greater than number of spatial streams');
end

% Lookup table for golay sequence generation
Wk_128 = [-1,-1,-1,-1,+1,-1,-1;
    +1,-1,-1,-1,+1,-1,-1;
    -1,-1,-1,+1,-1,-1,+1;
    +1,-1,-1,+1,-1,-1,+1;
    -1,-1,-1,+1,-1,+1,+1;
    +1,-1,-1,+1,-1,+1,+1;
    -1,-1,-1,+1,+1,+1,-1;
    +1,-1,-1,+1,+1,+1,-1];
Wk_sel = Wk_128(spatial_stream_i,:);

% Dk for 128-length sequences
Dk=[1 8 2 4 16 32 64];

% Each Ga and Gb will be 128 long if we're not doing channel bonding
% Generate Ga^i_128 field
% Generate Gb^i_128 field
[Ga,Gb]=generic_golay(Wk_sel, Dk, 128);

% Generate Gu^i_512
Gu=[-Gb -Ga  Gb -Ga];
% Generate Gv^i_512
Gv=[-Gb  Ga -Gb -Ga];

sequence_unit = [Gu Gv -Gb];

% We need to invert repetitions of the CEF depending 
% on the number of CEF repetitions
% EDMG STF and CEF Design for SC PHY in 11ay, slide 14
rep_invert_mat = [
    +1,+1,+1,+1,+1,+1,+1,+1;
    +1,+1,-1,-1,+1,+1,-1,-1;
    +1,+1,+1,+1,-1,-1,-1,-1;
    +1,+1,-1,-1,-1,-1,+1,+1
    ];
repetitions = max(ceil(log(n_spatial_streams)/log(2)),1);
rep_invert = repelem(rep_invert_mat(1:repetitions, spatial_stream_i), ...
                        1, length(sequence_unit));

sequence = rep_invert.*repmat(sequence_unit, 1, repetitions);

end

