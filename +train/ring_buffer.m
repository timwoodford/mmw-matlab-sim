% /*************************************************************************************
%
%    Project Name:  Golay Correlator
%    File Name:     ring_buffer.m
%    Authors:       Prem Kiran Nerella
%    Version:       1.0
%    History:       May 2014 created
%
%  *************************************************************************************
%    Description:
% 
%    function performs basic cyclic FIFO buffer operations like 'Create FIFO', 'Push Element to FIFO'
%    and 'Pop Element from FIFO'
%    
%    [rbOut dataOut] = ring_buffer(rbIn, dataIn, size, mode)
%
%    Inputs
%
%       1. rbIn   - cyclic FIFO buffer handle
%       2. dataIn - element to add to FIFO(associated with 'push' operation)
%       3. size   - FIFO size
%       4. mode   - operation descriptor 'init' or 'push' or 'pop'
%
%   Outputs
%   
%       1. rbOut  - cyclic FIFO buffer handle
%       2. dataOut- element read from FIFO(associated with 'pop' operation)
%   
%   Note        
%       1. rbIn , dataIn, dataOut are insignificant when property 'mode' is
%          set to 'init'
%          [rbOut []] = ring_buffer([], [], size, 'init')   
%
%       2. dataIn, is insignificant when property 'mode' is set to 'pop'
%          [rbOut []] = ring_buffer(rbIn, dataIn, size, 'push')    
%
%       3. dataOut, is insignificant when property 'mode' is set to 'push'
%          [rbOut dataOut] = ring_buffer(rbIn, [], size, 'pop')  
%
%   FIFO Handle elements description
%
%   buffer_start - specifies initial first element index of cyclic FIFO
%   buffer_end   - specifies initial last  element index of cyclic FIFO
%   data_start   - specifies current first element index of cyclic FIFO 
%   data_end     - specifies current last  element index of cyclic FIFO
%   count        - specifies current size of the cyclic FIFO
%   size         - specifies maximum size of the cyclic FIFO
%   data         - FIFO elements array
%
%  *************************************************************************************/
function [rb dataOut] = ring_buffer(rb, dataIn, size, mode)

% Ring FIFO Buffer
% mode-> 1:init 2:push 3:pop 4:free

if strcmpi(mode,'init')
    %FIFO handle
    rb = [];
    rb.buffer_start = [];
    rb.buffer_end = [];
    rb.data_start = [];
    rb.data_end = [];
    rb.count = [];
    rb.size = [];
    rb.data = [];
    
    %Data Type
    dType = 'double';
    %Initialize
    rb.data = zeros(1,size,dType);
    rb.buffer_start = 1;
    rb.buffer_end = size;
    rb.size = size;
    rb.data_start = 1;
    rb.data_end = 1;
    rb.count = 0;
    dataOut = [];

elseif strcmpi(mode,'push')
    %perform cyclic push operation
    %update data_end and count of FIFO handle
    rb.data(rb.data_end) = dataIn;
    rb.data_end = rb.data_end + 1;
    if (rb.data_end == (rb.buffer_end + 1))
        rb.data_end = rb.buffer_start;
    end    
    if (rb.count == rb.size)
        if ((rb.data_start + 1) == rb.buffer_end)
            rb.data_start = rb.buffer_start;
        else
            rb.data_start = rb.data_start + 1;
        end
    else
        rb.count = rb.count + 1;
    end
    dataOut = [];
elseif strcmpi(mode,'pop')    
    %perform cyclic pop operation
    %update data_start and count of FIFO handle
    dataOut = rb.data(rb.data_start);
    rb.data_start = rb.data_start + 1;
    if (rb.data_start == (rb.buffer_end + 1))
        rb.data_start = rb.buffer_start;
    end
    rb.count = rb.count - 1;
    if(rb.count < 0)
        display('Buffer is empty');
    end
elseif strcmpi(mode,'free')
    %free buffer(insignificant)
    rb = [];
    dataOut = [];
end

return
