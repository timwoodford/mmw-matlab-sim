function seq = pss_5gnr(NID1, NID2)
%TRAIN_5GNR_PSS Generate an PSS beam training signal
%   Detailed explanation goes here

% So here's the deal: we want to generate the PSS and/or SSS
% Each is a 127-carrier OFDM signal
% PSS goes in time slot 0 and SSS goes in time slot 2
% (See 7.4.3.1 in TS 38 211)
% The actual signal sequence comes from 7.4.2
NIDc = 3*NID1 + NID2;

% Pre-calculate x-sequence
x=[0 1 1 0 1 1 1];
for i=1:128-7
    x(i+7)=mod(x(i+4)+x(i), 2);
end

% Calculate x indices
m=mod(0:127+43*NIDc, 128)+1;

seq=1-2*x(m);

end

