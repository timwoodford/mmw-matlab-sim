function r=getchanest_80211ad()
% This corresponds to the OFDM channel estimation preamble
% Making it correspond to the SC preamble would be trivial
% Get long channel estimation field for 802.11ad directional mode
% 802.11ad-2012 spec 21.3.6.3 (pg. 479)

ga128_f = fopen('ga128.txt','r');
ga = fscanf(ga128_f, '%i'); % Copy/pasted from 802.11ad-2012 21.11
fclose(ga128_f); % Very important!

gb128_f = fopen('gb128.txt','r');
gb = fscanf(gb128_f, '%i'); % Copy/pasted from 802.11ad-2012 21.11
fclose(gb128_f); % Very important!

Gu=[-gb; -ga; gb; -ga];
Gv=[-gb; ga; -gb; -ga];

r=[Gv; Gu; Gv];

end