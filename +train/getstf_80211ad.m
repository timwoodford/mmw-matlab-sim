function r=getstf_80211ad()
% 802.11ad-2012 spec 21.3.6.2 (pg. 479) is what we're implementing
% 802.11ad-2012 21.4.3.1.2 has a different STF (I guess it's for the DMG
% control PHY only, although I have no idea why it's different for the
% control PHY.

% By spec, the STF is composed of 16 repetitions of sequences Ga128(n),
% which is defined in 21.11
% This is followed by a single repetition of -Ga128
% r_{STF}(nTc) = 
%        Ga128(n mod 128) * exp(j*pi*n/2)   n=0,1,...,16*128-1
%        -Ga128(n mod 128) * exp(j*pi*n/2)  n=16*128,...17*128-1

ga128 = [+1 +1 -1 -1 -1 -1 -1 -1 -1 +1 -1 +1 +1 -1 -1 +1 +1 +1 -1 -1 +1 ...
    +1 +1 +1 -1 +1 -1 +1 -1 +1 +1 -1 -1 -1 +1 +1 +1 +1 +1 +1 +1 -1 +1 -1 ...
    -1 +1 +1 -1 +1 +1 -1 -1 +1 +1 +1 +1 -1 +1 -1 +1 -1 +1 +1 -1 +1 +1 -1 ...
    -1 -1 -1 -1 -1 -1 +1 -1 +1 +1 -1 -1 +1 +1 +1 -1 -1 +1 +1 +1 +1 -1 +1 ...
    -1 +1 -1 +1 +1 -1 +1 +1 -1 -1 -1 -1 -1 -1 -1 +1 -1 +1 +1 -1 -1 +1 -1 ...
    -1 +1 +1 -1 -1 -1 -1 +1 -1 +1 -1 +1 -1 -1 +1];

ii=0:(17*128-1);

r=[repmat(ga128, 1, 16) -ga128].*exp(-1j*pi/2*ii);

end