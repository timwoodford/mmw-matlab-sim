function [ltf, ltf_t, ltf_f] = l_ltf(circ_shift)
% Legacy LTF from 802.11-2016
if nargin<1
    circ_shift = 0;
end

% 802.11-2016 19.3.9.3.4 (19-11)
ltf_f = [0 1 -1 -1 1 1 -1 1 -1 1 -1 -1 -1 -1 -1 1 1 -1 -1 1 -1 1 -1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 1 1 -1 -1 1 1 -1 1 -1 1 1 1 1 1 1 -1 -1 1 1 -1 1 -1 1 1 1 1];
ltf_t = ifft(ltf_f, 64);

ltf_t = ltf_t(mod((0:63)+circ_shift, 64)+1);

ltf = [ltf_t(33:64) ltf_t ltf_t];