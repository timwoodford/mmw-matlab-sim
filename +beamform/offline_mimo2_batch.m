function offline_mimo2_batch(ssw_file, ex_file, sw_range, out_file, ITER_MAX)

load(ssw_file, 'rx_sw_rss', 'tx_sw_rss', 'rx_sw_snr', 'tx_sw_snr');
load(ex_file, 'all_capacities_mab', 'all_capacities_zf', 'all_capacities_mmse', 'all_snrs', 'all_chan_conditions', 'N_CB', 'Fs');

rx_sw_rss=rx_sw_rss(:,:,sw_range);
rx_sw_snr=rx_sw_snr(:,:,sw_range);
tx_sw_rss=tx_sw_rss(:,:,sw_range);
tx_sw_snr=tx_sw_snr(:,:,sw_range);
% Set SNR NaNs -> a low number to avoid problems later
rx_sw_snr(isnan(rx_sw_snr))=-99;
rx_sw_snr(isnan(tx_sw_snr))=-99;

% Offline comparison of MIMO methods for exhaustive collection
% clearvars
% load('data/mimo/los/collect_mimo2_exhaustive_slow_190725-151408.mat', '-regexp', '^all_(.*)')
% load('data/mimo/abcxyz/full_ssw_190000-123456.mat');

all_capacities_mab(isnan(all_capacities_mab))=0;
all_capacities_zf(isnan(all_capacities_zf))=0;
all_capacities_mmse(isnan(all_capacities_mmse))=0;

% There are a number of options we can use here: the most obvious
% (used here) is to take the mean from both quasi-omni elements
tx_ssw = squeeze(mean(tx_sw_snr, 2));
rx_ssw = squeeze(mean(rx_sw_snr, 1));

% Reshape capacity data such that we have (rx 1,2,..., tx 1,2,...)
shaped_cap_mab = reshape(all_capacities_mab, N_CB, N_CB, N_CB, N_CB);
shaped_cap_zf = reshape(all_capacities_zf, N_CB, N_CB, N_CB, N_CB);
shaped_cap_mmse = reshape(all_capacities_mmse, N_CB, N_CB, N_CB, N_CB);
shaped_chan_c = reshape(all_chan_conditions, N_CB, N_CB, N_CB, N_CB);
shaped_snrs = reshape(all_snrs, N_CB, N_CB, N_CB, N_CB)/2;

% Convert SNR and channel condition to MIMO capacity
% See IEEE 802.11-15/0334r1

% MIMO capacity: \sum_i \log(1+P_i/N \sigma_i^2)
% We set P_i = P for non-waterfilling case (still-better capacity may be
% achievable with waterfilling, but I'll consider that beyond the scope of
% this script for now)
% Next, we need to determine the singular values of the channel matrix from
% the channel condition.  We want the squared singular values to sum to 2
sval1 = 2./(1+shaped_chan_c.^2);
mimo_cap = Fs * (log2(1+sval1.*shaped_snrs)+log2(1+(sval1.*shaped_chan_c.^2).*shaped_snrs));
% Ideal diversity = 9 dB SNR gain (4x from transmit without power constraint, 2x from rx)
diversity_cap = Fs*log2(1+8*shaped_snrs);


% Basic comparisons

max_mmse = max(all_capacities_mmse, [], 'all');
max_zf = max(shaped_cap_zf, [], 'all');
max_mimo = max(mimo_cap, [], 'all');
max_mab = max(diversity_cap, [], 'all');
max_all = max(max_mmse, max(max_zf, max_mab));
assert(all(abs(diversity_cap - shaped_cap_mab)<1e3, 'all'));

%%%%%%%%%%%% Evolutionary %%%%%%%%%%%%

SAMPLES = 128;

% Beamforming
cap_mab_e = zeros(1, ITER_MAX);
parfor iter_max = 1:ITER_MAX
    for jj=1:SAMPLES
        [~, val] = beamform.sumimo_evolutionary(rx_ssw, tx_ssw, diversity_cap, iter_max);
        cap_mab_e(iter_max) = cap_mab_e(iter_max) + val;
    end
    cap_mab_e(iter_max) = cap_mab_e(iter_max)/SAMPLES;
end


% Multiplexing
cap_zf_e = zeros(1, ITER_MAX);
cap_mmse_e = zeros(1, ITER_MAX);
cap_mimo_e = zeros(1, ITER_MAX);
parfor iter_max = 1:ITER_MAX
    for jj=1:SAMPLES
        [~, val] = beamform.sumimo_evolutionary(rx_ssw, tx_ssw, shaped_cap_zf, iter_max);
        cap_zf_e(iter_max) = cap_zf_e(iter_max) + val;
        [~, val] = beamform.sumimo_evolutionary(rx_ssw, tx_ssw, mimo_cap, iter_max);
        cap_mimo_e(iter_max) = cap_mimo_e(iter_max) + val;
        [~, val] = beamform.sumimo_evolutionary(rx_ssw, tx_ssw, shaped_cap_mmse, iter_max);
        cap_mmse_e(iter_max) = cap_mmse_e(iter_max) + val;
    end
    cap_zf_e(iter_max) = cap_zf_e(iter_max)/SAMPLES;
    cap_mimo_e(iter_max) = cap_mimo_e(iter_max)/SAMPLES;
    cap_mmse_e(iter_max) = cap_mmse_e(iter_max)/SAMPLES;
end

%%%%%%%%%%%% K-best %%%%%%%%%%%%

% Beamforming
cap_mab_k = zeros(1, ITER_MAX);
TRY_IDX = 1:ITER_MAX;
for iter_max = TRY_IDX
    [~, cap_mab_k(iter_max)] = beamform.sumimo_k_best_2(rx_ssw, tx_ssw, diversity_cap, iter_max);
end

% Multiplexing
cap_zf_k = zeros(1, ITER_MAX);
cap_mmse_k = zeros(1, ITER_MAX);
cap_mimo_k = zeros(1, ITER_MAX);
for iter_max = TRY_IDX
    [~, cap_zf_k(iter_max)] = beamform.sumimo_k_best_2(rx_ssw, tx_ssw, shaped_cap_zf, iter_max);
    [~, cap_mmse_k(iter_max)] = beamform.sumimo_k_best_2(rx_ssw, tx_ssw, shaped_cap_mmse, iter_max);
    [~, cap_mimo_k(iter_max)] = beamform.sumimo_k_best_2(rx_ssw, tx_ssw, mimo_cap, iter_max);
end

% The original k-best implementation has better early performance sometimes
% for reasons still unclear to me
[~, cap_zf_k(1)] = beamform.sumimo_k_best(rx_ssw, tx_ssw, shaped_cap_zf, 1);
[~, cap_mmse_k(1)] = beamform.sumimo_k_best(rx_ssw, tx_ssw, shaped_cap_mmse, 1);
[~, cap_mimo_k(1)] = beamform.sumimo_k_best(rx_ssw, tx_ssw, mimo_cap, 1);

save(out_file, 'max_mmse', 'max_zf', 'max_mimo', 'max_mab', 'cap_mab_e', ...
    'cap_zf_e', 'cap_mab_k', 'cap_zf_k', 'cap_mimo_e', 'cap_mmse_e', ...
    'cap_mmse_k', 'cap_mimo_k');

