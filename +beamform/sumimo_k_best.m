function [sel_idx, sum_rate] = sumimo_k_best(rx_ssw, tx_ssw, capacity_mat, k)
% SUMIMO_K_BEST Get the k-best result, given SNR and RSS data from an
% exhaustive channel estimate

% SNR, RSS matrix formats:
% (PA 1 idx, PA 2 idx, ...)
% Capacity matrix format:
% (rx CB idx 0, 1, ..., tx CB idx 0, 1, ...)

% Find k-best tx and rx
% Pick the beams with the highest metric for each PA.  If there's a
% pattern that will allow us to easily find a channel with better rank, we
% might consider trying that as well
% Since we have multiple PAs listening on both tx and rx sides, let's say
% that our SSW method will involve taking the mean of the score across all
% PAs.  The alternate method would be to pair off the PAs in advance.
[~, tx_best_idx] = maxk(tx_ssw, k, 2);
[~, rx_best_idx] = maxk(rx_ssw, k, 2);

% The next step is to try all possible combinations given above and look
% for the combination that gives the best sum rate
idx_vec = [rx_best_idx' tx_best_idx'];
% Warning: wild Matlab code ahead
assert(size(idx_vec,1)==k);

all_combos = idx_vec;
for ii=1:size(idx_vec, 2)-1
    all_combos = repmat(all_combos, k, 1);
    sz_1 = size(all_combos,1);
    for jj=1:ii
        all_combos(:,jj) = repelem(all_combos(1:sz_1/k,jj),k);
    end
end

% Use linear indexing - this is basically an adaptation of sub2ind
idx_mapper = cumprod(size(capacity_mat));
idx_mapper = [1 idx_mapper(1:end-1)];
opt_submat = capacity_mat(idx_mapper*(all_combos-1)' + 1);

[sum_rate, sum_idx] = max(reshape(opt_submat, numel(opt_submat), 1));

sel_idx = all_combos(sum_idx,:);





