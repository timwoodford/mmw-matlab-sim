
function capacity = estimate_total_capacity(payload, expected, Fs)
% ESTIMATE_TOTAL_CAPACITY Estimate total MIMO capacity by calculating SNRs
nchan = size(payload,1);
snrs = zeros(1,nchan);
for chan_ii=1:nchan
    this_payload = payload(chan_ii,:,:);
    this_expected = expected(chan_ii,:,:);
    evm_mat = abs(this_payload - this_expected).^2;
    aevms = mean(evm_mat(:));
    snrs(chan_ii) = 1./aevms;
end
capacity = Fs*sum(log2(1+snrs));
end