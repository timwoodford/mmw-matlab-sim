function [sel_idx, sum_rate] = sumimo_evolutionary(rx_ssw, tx_ssw, capacity_mat, term_idx)

[sel_idx, sum_rate] = beamform.sumimo_k_best(rx_ssw, tx_ssw, capacity_mat, 1);

rx_best_idx = sel_idx(1:size(rx_ssw,1));
tx_best_idx = sel_idx((size(rx_ssw,1)+1):end);
best_rate = get_val(capacity_mat, [rx_best_idx tx_best_idx]);
assert(best_rate == sum_rate);

for ii=1:term_idx
    % Select new rx and tx indices based on a PDF derived from the SSWs
    new_rx = zeros(1, size(rx_ssw,1));
    for rx_ii=1:length(new_rx)
        new_rx(rx_ii) = draw_from_distribution(rx_ssw(rx_ii,:));
    end
    new_tx = zeros(1, size(tx_ssw,1));
    for tx_ii=1:length(new_tx)
        new_tx(tx_ii) = draw_from_distribution(tx_ssw(tx_ii,:));
    end
    if any(new_tx > size(rx_ssw, 2))
        error('hi');
    end
    % Try the new rx configuration
    rx_try = get_val(capacity_mat, [new_rx tx_best_idx]);
    if rx_try > best_rate
        rx_best_idx = new_rx;
        best_rate = rx_try;
    end
    % Try the new tx configuration
    tx_try = get_val(capacity_mat, [rx_best_idx new_tx]);
    if tx_try > best_rate
        tx_best_idx = new_tx;
        best_rate = tx_try;
    end
    % Evolutionary algorithm is unclear on whether we update the SSW PDF
    % based on these additional measurements.  It's not entirely clear how
    % we should go about this, so I'll avoid this approach for now
end

sel_idx = [rx_best_idx tx_best_idx];
sum_rate = best_rate;

end

function y = get_val(matrix, indices)
idx_mapper = cumprod(size(matrix));
idx_mapper = [1 idx_mapper(1:end-1)];
y = matrix(idx_mapper*(indices-1)'+1);
end

function sel_idx = draw_from_distribution(distr)
% Convert distribution so minimum >= 0
distr = distr - min(distr) + 1e-1;
C = cumsum(distr);
sel_idx = 1+sum(C(end)*rand>C);
end