function [sel_idx, sum_rate] = sumimo_k_best_2(rx_ssw, tx_ssw, capacity_mat, k)
% SUMIMO_K_BEST Get the k-best result, given SNR and RSS data from an
% exhaustive channel estimate

% SNR, RSS matrix formats:
% (PA 1 idx, PA 2 idx, ...)
% Capacity matrix format:
% (rx CB idx 0, 1, ..., tx CB idx 0, 1, ...)

% Find k-best tx and rx
% Pick the beams with the highest metric for each PA.  If there's a
% pattern that will allow us to easily find a channel with better rank, we
% might consider trying that as well
% Since we have multiple PAs listening on both tx and rx sides, let's say
% that our SSW method will involve taking the mean of the score across all
% PAs.  The alternate method would be to pair off the PAs in advance.
% [~, tx_best_idx] = maxk(tx_ssw, k, 2);
% [~, rx_best_idx] = maxk(rx_ssw, k, 2);
ssws = [rx_ssw; tx_ssw];
n_cb = size(ssws, 2);
total_pas = size(ssws, 1);
[ssws_d, ssws_i] = sort(ssws,2,'descend');
ssws_d = ssws_d - max(ssws, [], 2);
ssws_d = [ssws_d -9e9*ones(size(ssws_d,1),1)];
descent_amounts = zeros(1,size(ssws_d, 1));
descent_idx = ones(1,size(ssws_d, 1));
if any(isnan(ssws_d))
    error('????');
end

while prod(descent_idx) < k
    % This next line uses linear indexing (that's why we multiply by
    % total_pas)
    estimated_degredation = abs(ssws_d((1:total_pas) + (descent_idx-1+1)*total_pas)) + descent_amounts;
    [amount, idx_sel] = min(estimated_degredation + 9e9*(descent_idx == n_cb));
    if amount > 9e8
        k = prod(descent_idx);
        break
    end
    descent_amounts(idx_sel) = descent_amounts(idx_sel) + amount;
    descent_idx(idx_sel) = descent_idx(idx_sel) + 1;
end

test_idx = combinations_idxs(descent_idx, k);
test_combos = zeros(size(test_idx));
for ii=1:size(test_idx, 1)
    test_combos(ii,:) = ssws_i(ii, test_idx(ii,:));
end

% Use linear indexing - this is basically an adaptation of sub2ind
idx_mapper = cumprod(size(capacity_mat));
idx_mapper = [1 idx_mapper(1:end-1)];
opt_submat = capacity_mat(idx_mapper*(test_combos-1) + 1);

[sum_rate, sum_idx] = max(reshape(opt_submat, numel(opt_submat), 1));

sel_idx = test_combos(:,sum_idx)';


end

function all_combos = combinations(input_vals)
% COMBINATIONS Generate all combinations of a set of beam patterns

all_combos = input_vals;
num_choices = size(input_vals, 2);
num_options = size(input_vals, 1);
for ii=1:num_choices-1
    all_combos = repmat(all_combos, num_options, 1);
    sz_1 = size(all_combos,1);
    for jj=1:ii
        all_combos(:,jj) = repelem(all_combos(1:sz_1/num_options,jj),num_options);
    end
end

end

function all_combos = combinations_idxs(max_idx, k)
% COMBINATIONS Generate all combinations of a set of beam patterns

num_choices = length(max_idx);
all_combos = zeros(num_choices, k);
for ii=1:k
    all_combos(:,ii) = myind2sub(max_idx, ii);
end

end

function sub = myind2sub(sz, ind)

k = cumprod(sz);
for i = length(sz):-1:2
    vi = rem(ind-1, k(i-1)) + 1;
    vj = (ind - vi)/k(i-1) + 1;
    sub(i) = double(vj);
    ind = vi;
end
sub(1) = vi;

end