%% Scenario generation parameters
N_CLIENT_PA = 1; % Collected data gives us 2 client PAs.  We may reduce this number
N_SCENARIOS = 50; % The number of randomised client combinations to create
DATA_DIR = 'data/mobility-indoor-wide';
BEAM_CFG = 'wide';

%% Data loading
ENTRIES=0:8;
load(sprintf('%s/%s_%d.mat', DATA_DIR, BEAM_CFG, ENTRIES(1)), 'N_CB');

chan_matrices = zeros(length(ENTRIES), N_CB^2, N_CB^2, 2, 2);
chan_snrs = zeros(length(ENTRIES), N_CB^2, N_CB^2);

for entry_idx=1:length(ENTRIES)
    entry=ENTRIES(entry_idx);
    % TODO check that it's a mat-file
    entry_f = load(sprintf('%s/%s_%d.mat', DATA_DIR, BEAM_CFG, entry));
    chans_in = entry_f.all_channels;
    ch_sz = size(chans_in);
    chans_out = zeros(ch_sz(1:4));
    for ii=1:size(chans_in, 1)
        for jj=1:size(chans_in, 2)
            ch = squeeze(chans_in(ii,jj,:,:,:));
            ofdm_avg = mean(ch, [1 2]);
            % I think we need to normalize based on frequency bins to avoid
            % accidentally cancelling an entry due to phase differences
            chans_out(ii,jj,:,:) = mean(ch./ofdm_avg*mean(abs(ch), 'all'), 3, 'omitnan');
            % Use nanmean to deal with zero frequency bins around DC
        end
    end
    chan_matrices(entry_idx,:,:,:,:) = chans_out;
    chan_snrs(entry_idx,:,:) = entry_f.all_snrs;
end
num_meas = size(chan_matrices, 1);

% Deal with NaNs
chan_matrices(isnan(chan_matrices))=0;


%% Create randomised scenarios
% Our outputs will be a matrix with indices:
% * Scenario index
% * 2: rx/tx indices
% * 2: channel matrix

scenarios = zeros(N_SCENARIOS, size(chan_matrices,2), size(chan_matrices,3), 2, 2*N_CLIENT_PA);
sc_sel = randi(num_meas, 2, N_SCENARIOS);
for sc_idx=1:N_SCENARIOS
    % TODO make sure this matrix index is correct
    sc_1_m = squeeze(chan_matrices(sc_sel(1,sc_idx),:,:,:,:));
    sc_1_s = squeeze(chan_snrs(sc_sel(1,sc_idx),:,:));
    sc_2_m = squeeze(chan_matrices(sc_sel(2,sc_idx),:,:,:,:));
    sc_2_s = squeeze(chan_snrs(sc_sel(2,sc_idx),:,:));
    for ii=1:N_CB^2
        for jj=1:N_CB^2
            chan_1 = sc_1_m(ii,jj,:,:);
            chan_1 = mean(squeeze(chan_1/mean(abs(chan_1), 'all')*sc_1_s(ii,jj)),2,'omitnan');
            chan_2 = sc_2_m(ii,jj,:,:);
            chan_2 = mean(squeeze(chan_2/mean(abs(chan_2), 'all')*sc_2_s(ii,jj)),2,'omitnan');
            scenarios(sc_idx,ii,jj,:,:) = [chan_1 chan_2];
        end
    end
end

% Deal with NaNs
scenarios(isnan(scenarios))=0;

%% Find global best option

assert(N_CLIENT_PA==1); % makes my life easy
% Method 1: no ZF - just use phased arrays
% Method 2: MMSE at the AP: log(det(I+HH^H))
% would normally need to multiply by SNR, but we've already done that

pa_only_caps = zeros(1, N_SCENARIOS);
zf_caps = zeros(1, N_SCENARIOS);
for sc_idx=1:N_SCENARIOS
    scenario = squeeze(scenarios(sc_idx, :,:,:,:));
    cap_metric = zeros(size(scenario,1), size(scenario,2));
    cap_metric_zf = zeros(size(scenario,1), size(scenario,2));
    tic
    for ii1=1:N_CB^2
        for jj1=1:N_CB^2
            % We vectorize half of the operations because vectorizing
            % everything causes OOM errors with N_CB=16
            %H1=(scenario(ii1,jj1,:,:));
            %H2=(scenario(ii2,jj2,:,:));
            %sinr_1 = abs(H1(1,1,1,1))./abs(H2(1,1,2,1));
            %sinr_2 = abs(H2(1,1,2,2))./abs(H1(1,1,1,2));
            %sinr_1 = abs(scenario(ii1,jj1,1,1))./abs(scenario(ii2,jj2,2,1));
            %sinr_2 = abs(scenario(ii2,jj2,2,2))./abs(scenario(ii1,jj1,1,2));
            sinr_1 = abs(scenario(ii1,jj1,1,1))./(1+abs(scenario(:,:,2,1)));
            sinr_2 = abs(scenario(:,:,2,2))./(1+abs(scenario(ii1,jj1,1,2)));
            cap_metric(ii1,jj1) = max(log2(1+sinr_1) + log2(1+sinr_2),[],'all');
            % For ZF/MMSE capacity...
            % det(I+HH^H) =
            % (1+|h11|^2+|h12|^2)(1+|h22|^2+|h21|^2)-abs(h11h21*+h22h12*)^2
            % ^ calculations done on paper I have somewhere
            % h11 = h(ii1,jj1,1,1), h12=h(ii2,jj2,1,2), h21=h(ii1,jj1,2,1),
            % h22 = h(ii2,jj2,2,2), ii2 and jj2 are vectorized
            zf_det_vals = ...
                (1+abs(scenario(ii1,jj1,1,1)).^2+abs(scenario(:,:,1,2)).^2) ...
                .*(1+abs(scenario(:,:,2,2)).^2+abs(scenario(ii1,jj1,2,1)).^2) ...
                - abs(scenario(ii1,jj1,1,1).*conj(scenario(ii1,jj1,2,1)) ...
                + scenario(:,:,2,2).*conj(scenario(:,:,1,2))).^2;
            cap_metric_zf = max(zf_det_vals,[],'all');
        end
    end
    toc
    pa_only_caps(sc_idx) = max(cap_metric, [], 'all');
    zf_caps(sc_idx) = log2(max(cap_metric_zf, [], 'all'));
end

%% Find heuristic best option
% Right now, I think that many-to-many beam alignment (Jog et al) may have
% the best-available heuristic algorithm for what we're doing (which is
% focused on multiplexing rather than scheduling)

%% Save data
SRC_SCRIPT=mfilename;
[~,GIT_VER] = system('git rev-parse HEAD');
GIT_VER = GIT_VER(1:end-1);
save([SRC_SCRIPT '_' datestr(datetime('now'), 'yymmdd-HHMMSS') '.mat']);
