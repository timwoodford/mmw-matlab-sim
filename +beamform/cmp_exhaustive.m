clearvars
cmp1 = load('collect_mimo2_exhaustive_slow_190726-232745.mat');
cmp2 = load('collect_mimo2_exhaustive_slow_190727-010138.mat');

figure(1); compare_scatter(cmp1.all_capacities_mab, cmp2.all_capacities_mab);
title('MAB Capacity Comparison');


figure(2); compare_scatter(cmp1.all_chan_conditions, cmp2.all_chan_conditions);
title('Channel Condition Consistency');

figure(3); compare_scatter(cmp1.all_capacities_zf, cmp2.all_capacities_zf);
title('Zero-forcing Capacity Consistency');

function compare_scatter(vals1, vals2)

scatter(reshape(vals1, 1, numel(vals1)), reshape(vals2, 1, numel(vals2)));
xlabel('Measured Value - Run 1');
ylabel('Measured Value - Run 2');

end