function [selected_tx, selected_rx, beam_snrs] = exhaustive_sumimo(beam_measurements, n_tx_pa, n_rx_pa)
% Beam measurements are indexed (tx_i, rx_i), where tx_i= N_cb*PA_i + CB_i 

% Pretty sure this is a combinatorial problem
% Thankfully, it should just be O(N!), where N is the number of phased arrays,
% which should be relatively small

n_tx_beam = size(beam_measurements, 1);
n_rx_beam = size(beam_measurements, 2);

n_cb = n_tx_beam/n_tx_pa;
assert(mod(n_cb,1)==0 && n_cb*n_tx_pa == n_tx_beam);

max_num_streams = min(n_tx_pa, n_rx_pa);
beam_tx_choices = nchoosek((1:n_tx_beam), max_num_streams);
beam_rx_choices = nchoosek((1:n_rx_beam), max_num_streams);

% For now, we'll try to do the maximum number of streams possible
% However, the diversity vs. streams tradeoff might be interesting
best_option = {-1, 0, 0, []}; % sum SIR, tx_beams, rx_beams, SIR
for tx_ii=1:size(beam_tx_choices, 1)
    tx_beams = beam_tx_choices(tx_ii, :);
    for rx_ii=1:size(beam_rx_choices, 1)
        % Now we need to examine all pairings of the selected rx and tx 
        % beams.  We can do this by finding all permutations of the 
        % ordering of the rx beams
        rx_beam_perms = perms(beam_rx_choices(rx_ii, :));
        for perm_ii=1:size(rx_beam_perms, 1)
            rx_beams = rx_beam_perms(perm_ii,:);
            assert(length(rx_beams)==length(tx_beams) && length(rx_beams)==max_num_streams);
            % Make sure we aren't using any 1 phased array twice
            if check_duplicate_pa(tx_beams, n_cb) || check_duplicate_pa(rx_beams, n_cb)
                continue
            end
            % Calculate sum-SIR
            for beam_ii=1:max_num_streams
                beam_pwr = beam_measurements(rx_beams(beam_ii), tx_beams(beam_ii));
                beam_ifer = pow2db(sum(db2pow(beam_measurements(rx_beams(beam_ii), tx_beams))) - db2pow(beam_pwr));
                beam_ifer = max(beam_ifer, 0);
                beam_sir(beam_ii) = beam_pwr - beam_ifer;
            end
            % Check for improvement
            sum_sir = sum(beam_sir);
            if sum_sir > best_option{1}
                best_option{1} = sum_sir;
                best_option{2} = tx_beams;
                best_option{3} = rx_beams;
                best_option{4} = beam_sir;
            end
        end
    end
end

selected_tx = best_option{2};
selected_rx = best_option{3};
beam_snrs = beam_measurements(selected_tx, selected_rx);

end

function duped = check_duplicate_pa(selections, n_cb)
% Check if any PA is being used twice
duped = false;
if length(selections)==1
    return
end
n_pa = ceil(max(selections)/n_cb);
selections = selections - 1;
for pa_ii=((1:n_pa)-1)
    if sum(selections >= (pa_ii*n_cb) & selections < ((pa_ii+1)*n_cb)) > 1
        duped = true;
        return
    end
end

end
