% close all
clearvars
BASE_DIR = 'data/mimo/mobility-indoor-wide';
MEAS_TYPE = 'wide';
N_PTS = 9;
MAX_K = 256;

mab_max_cap = zeros(1,N_PTS);
mimo_max_cap = zeros(1,N_PTS);
mmse_max_cap = zeros(1,N_PTS);
zf_max_cap = zeros(1,N_PTS);

% We'll compare MAB-k-best, MMSE-Ev, and MMSE-k-best
% MMSE performance is generally pretty similar to ZF, except that it
% performs better in low-SNR situations
mab_k = zeros(N_PTS, MAX_K);
mmse_k = zeros(N_PTS, MAX_K);
mmse_ev = zeros(N_PTS, MAX_K);

for ii=1:N_PTS
    mdat=load(sprintf('%s/proc_%s_%d.mat', BASE_DIR, MEAS_TYPE, ii-1));
    mab_max_cap(ii) = mdat.max_mab;
    mimo_max_cap(ii) = mdat.max_mimo;
    mmse_max_cap(ii) = mdat.max_mmse;
    zf_max_cap(ii) = mdat.max_zf;
    mab_k(ii,:) = mdat.cap_mab_k;
    mmse_k(ii,:) = mdat.cap_mmse_k;
    mmse_ev(ii,:) = mdat.cap_mmse_e;
    if 0
        figure;  plot([mdat.cap_mab_k; mdat.cap_mab_e; mdat.cap_zf_k; mdat.cap_zf_e; mdat.cap_mmse_k; mdat.cap_mmse_e]');
        if ii==1
            legend({'MAB k-best', 'MAB evolutionary', 'ZF k-best', 'ZF evolutionary', 'MMSE k-best', 'MMSE Evolutionary'});
        end
    end
end


figure(10); hold on; plot([mab_max_cap; mmse_max_cap; zf_max_cap]');
legend({'MAB', 'MMSE', 'ZF'});

% distFig('Rows', 2, 'Columns', 5);


% Capacity-with-overhead calculations
cap_mab_k = mean(mab_k, 1);
cap_mmse_k = mean(mmse_k, 1);
cap_mmse_e = mean(mmse_ev, 1);

% Time calculations, values taken from our system
cb_gap = 256;
time_per_meas = (cb_gap + 5*64 + 32)/125e6;
full_ssw_time = 4*16*time_per_meas;
train_times = time_per_meas*(1:length(cap_mmse_e)) + full_ssw_time;

% Calculate capacity with overhead
coherence_times = linspace(full_ssw_time, 40e-3, 1000);

mab_capacity = zeros(1, length(coherence_times));
mimo_k_capacity = zeros(1, length(coherence_times));
mimo_e_capacity = zeros(1, length(coherence_times));
for ii=1:length(coherence_times)
    cohere_tm = coherence_times(ii);
    caps_mab = (cohere_tm - train_times)/cohere_tm.*cap_mab_k;
    mab_capacity(ii) = max(caps_mab);
    % Despite the name, this is not actually zero-forcing...
    caps_k_mult = (cohere_tm - train_times)/cohere_tm.*cap_mmse_k;
    mimo_k_capacity(ii) = max(caps_k_mult);
    caps_e_mult = (cohere_tm - train_times)/cohere_tm.*cap_mmse_e;
    mimo_e_capacity(ii) = max(caps_e_mult);
end

figure(11); hold on; semilogx(coherence_times, [mab_capacity; mimo_k_capacity; mimo_e_capacity]');
legend({'Multi-array Beamforming', 'Multiplexing (k-best)', 'Multiplexing (Evolutionary)'});
axis tight;
xlabel('Coherence time');
ylabel('Capacity (Bps)');