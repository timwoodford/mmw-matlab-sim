Maximum transition time: 23 samples (184 ns) (about 4 periods of the sine wave)
Command delay: 25 samples, 3.08 stdev -> 200 ns, 24.6 stdev

  == Measurement set 1 ==
tx_tone_1.mat:
transition: avg=12.8927, std=3.0381, max=21
drift=7.7827e-6 drift samples/sample
delay to cmd completion: 21 samples, 168 ns (est)
Failure rate: 0.16%

tx_tone_2.mat
transition: avg=13.5796, std=21.5144, max=23
drift=7.7827e-6 drift samples/sample
delay to cmd completion: 26 samples, 208 ns (est)
Failure rate: 0.13%

tx_tone_short_1.mat
transition: avg=13.5835, std=3.1818, max=20
drift=8.0224e-6
delay to cmd completion: 23 samples, 184 ns (est)
Failure rate: 0.1%

tx_tone_short_2.mat
transition: avg=13.1376, std=2.8951, max=20
drift=7.9171e-6
delay to cmd completion: 26 samples, 208 ns
Failure rate: 0

tx_tone_short_3.mat
transition: avg=12.6861, std=2.7442, max=19
drift=7.8593e-6
delay to cmd completion: 29 samples, 232 ns
Failure rate: 0
