function tx_vector = assemble_cb_tx(transmit_mat, cb_idx, pad_len)

n_reps = length(cb_idx);
data_len = size(transmit_mat, 2);
tx_vector = zeros(1, n_reps*data_len + (n_reps+1)*pad_len, 'single');

for ii=1:n_reps
    tx_vector((1:data_len)+(ii-1/2)*pad_len+(ii-1)*data_len) = ...
        transmit_mat(cb_idx(ii),:);
end

end