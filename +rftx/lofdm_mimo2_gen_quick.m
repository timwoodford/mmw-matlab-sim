% Generate multiple datasets for 2x2 MIMO, one for each codebook entry
%
% WARP-derived code Copyright (c) 2015 Mango Communications - All Rights Reserved
% Distributed under the WARP License (http://warpproject.org/license)

%% Parameters
% OFDM params
SC_IND_PILOTS           = [8 22 44 58];                           % Pilot subcarrier indices
SC_IND_DATA             = [2:7 9:21 23:27 39:43 45:57 59:64];     % Data subcarrier indices
N_SC                    = 64;                                     % Number of subcarriers
CP_LEN                  = 16;                                     % Cyclic prefix length
N_DATA_SYMS             = N_OFDM_SYMS * length(SC_IND_DATA);      % Number of data symbols (one per data-bearing subcarrier per OFDM symbol)
TX_SPATIAL_STREAM_SHIFT = 3;                                      % Number of samples to shift the transmission from RFB
TX_SCALE                = 1;

%% Define a half-band 2x interpolation filter response
interp_filt2 = zeros(1,43);
interp_filt2([1 3 5 7 9 11 13 15 17 19 21]) = [12 -32 72 -140 252 -422 682 -1086 1778 -3284 10364];
interp_filt2([23 25 27 29 31 33 35 37 39 41 43]) = interp_filt2(fliplr([1 3 5 7 9 11 13 15 17 19 21]));
interp_filt2(22) = 16384;
interp_filt2 = interp_filt2./max(abs(interp_filt2));

%% Preamble

% STF
sts_f = zeros(1,64);
sts_f(1:27) = [0 0 0 0 -1-1i 0 0 0 -1-1i 0 0 0 1+1i 0 0 0 1+1i 0 0 0 1+1i 0 0 0 1+1i 0 0];
sts_f(39:64) = [0 0 1+1i 0 0 0 -1-1i 0 0 0 1+1i 0 0 0 -1-1i 0 0 0 -1-1i 0 0 0 1+1i 0 0 0];
sts_t = ifft(sqrt(13/6).*sts_f, 64);
sts_t = sts_t(1:16);

% LTF
lts_f = [0 1 -1 -1 1 1 -1 1 -1 1 -1 -1 -1 -1 -1 1 1 -1 -1 1 -1 1 -1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 1 1 -1 -1 1 1 -1 1 -1 1 1 1 1 1 1 -1 -1 1 1 -1 1 -1 1 1 1 1];
lts_t = ifft(lts_f, 64);

% To avoid accidentally beamforming the preamble transmissions, we will
% let RFA be dominant and handle the STS and first set of LTS. We will
% append an extra LTS sequence from RFB so that we can build out the
% channel matrix at the receiver

sts_t_rep = repmat(sts_t, 1, 30);

% We're not doing AGC, and the rx code uses LTS for timesync and CFO, so
% we'll take out the CFO for the sake of efficiency
% preamble_legacy_A = [sts_t_rep, lts_t(33:64), lts_t, lts_t];
% preamble_legacy_B = [circshift(sts_t_rep, [0, TX_SPATIAL_STREAM_SHIFT]), zeros(1, 160)];
preamble_legacy_A = [lts_t(33:64), lts_t, lts_t];
preamble_legacy_B = zeros(1, 160);

% MIMO Preamble

% There are many strategies for training MIMO channels. Here, we will use
% the LTS sequence defined before and orthogonalize over time. First we
% will send the sequence on stream A and then we will send it on stream B

% Basically, it will look like:
%   LTS   000   LTS   0000
%   000   LTS   000   Data
% We need to figure out how we want to do OFDM estimation with >2 streams

% preamble_mimo_A = [lts_t(33:64), lts_t, zeros(1,96)];
% preamble_mimo_B = [zeros(1,96), lts_t(33:64), lts_t];

preamble_mimo_A = [zeros(1,96), lts_t(33:64), lts_t];
preamble_mimo_B = [lts_t(33:64), lts_t, zeros(1,96)];

preamble_A = [preamble_legacy_A, preamble_mimo_A];
preamble_B = [preamble_legacy_B, preamble_mimo_B];

%% Data
% The idea here is to generate a separate data set for each codebook entry
% This will allow us to identify the current beam pattern without prior
% synchronization, hopefully
tx_data = randi(MOD_ORDER, N_CB, N_DATA_SYMS) - 1;

tx_syms_space_mat = (2*tx_data - 1)/sqrt(2);

% Split the tx data in half - first half goes to A, second half to B
% Note that WARP does the splitting differently
tx_syms = tx_syms_space_mat(:,1:N_DATA_SYMS);
% tx_syms_B = tx_syms_space_mat(:,(1:N_DATA_SYMS/2)+N_DATA_SYMS/2);

%% OFDM Stuff

% Define the pilot tone values as BPSK symbols
%  We will transmit pilots only on RF A
%  (This may not work for some MU-MIMO scenarios)
% pilots_A = [1 1 -1 1].';
% pilots_B = [0 0 0 0].';
pilots_A = [ 0  0  0  0].';
pilots_B = [+1 +1 -1 +1].';
% Repeat the pilots across all OFDM symbols
pilots_mat_A = repmat(pilots_A, 1, N_OFDM_SYMS);
pilots_mat_B = repmat(pilots_B, 1, N_OFDM_SYMS);

% It is at this point that I think we have to put everything into a loop
for cb_ii=1:size(tx_data, 1)
    % Reshape the symbol vector to a matrix with one column per OFDM symbol
    tx_syms_mat_A = zeros(length(SC_IND_DATA), N_OFDM_SYMS);
    tx_syms_mat_B = reshape(tx_syms(cb_ii,:), length(SC_IND_DATA), N_OFDM_SYMS);
    
    
    % Construct the IFFT input matrix
    ifft_in_mat_A = zeros(N_SC, N_OFDM_SYMS);
    ifft_in_mat_B = zeros(N_SC, N_OFDM_SYMS);
    
    % Insert the data and pilot values; other subcarriers will remain at 0
    ifft_in_mat_A(SC_IND_DATA, :)   = tx_syms_mat_A;
    ifft_in_mat_A(SC_IND_PILOTS, :) = pilots_mat_A;
    
    ifft_in_mat_B(SC_IND_DATA, :)   = tx_syms_mat_B;
    ifft_in_mat_B(SC_IND_PILOTS, :) = pilots_mat_B;
    
    %Perform the IFFT
    tx_payload_mat_A = ifft(ifft_in_mat_A, N_SC, 1);
    tx_payload_mat_B = ifft(ifft_in_mat_B, N_SC, 1);
    
    % Insert the cyclic prefix
    if(CP_LEN > 0)
        tx_cp = tx_payload_mat_A((end-CP_LEN+1 : end), :);
        tx_payload_mat_A = [tx_cp; tx_payload_mat_A];
        
        tx_cp = tx_payload_mat_B((end-CP_LEN+1 : end), :);
        tx_payload_mat_B = [tx_cp; tx_payload_mat_B];
    end
    
    % Reshape to a vector
    tx_payload_vec_A = reshape(tx_payload_mat_A, 1, numel(tx_payload_mat_A));
    tx_payload_vec_B = reshape(tx_payload_mat_B, 1, numel(tx_payload_mat_B));
    
    % Construct the full time-domain OFDM waveform
    tx_vec_A = [preamble_A tx_payload_vec_A];
    tx_vec_B = [preamble_B tx_payload_vec_B];
    
    % Pad with zeros for transmission - reduce these to fit size
    tx_vec_padded_A = [tx_vec_A zeros(1,1)];
    tx_vec_padded_B = [tx_vec_B zeros(1,1)];
    
    %% Interpolate
    if(INTERP_RATE == 1)
        tx_vec_air_A = tx_vec_padded_A;
        tx_vec_air_B = tx_vec_padded_B;
    elseif(INTERP_RATE == 2)
        % Zero pad then filter (same as interp or upfirdn without signal processing toolbox)
        tx_vec_2x_A = zeros(1, 2*numel(tx_vec_padded_A));
        tx_vec_2x_A(1:2:end) = tx_vec_padded_A;
        tx_vec_air_A = filter(interp_filt2, 1, tx_vec_2x_A);
        tx_vec_2x_B = zeros(1, 2*numel(tx_vec_padded_B));
        tx_vec_2x_B(1:2:end) = tx_vec_padded_B;
        tx_vec_air_B = filter(interp_filt2, 1, tx_vec_2x_B);
    end
    
    % Scale the Tx vector to +/- 1
    tx_vec_air_A = TX_SCALE .* tx_vec_air_A ./ max(abs(tx_vec_air_A));
    tx_vec_air_B = TX_SCALE .* tx_vec_air_B ./ max(abs(tx_vec_air_B));
    
    
    TX_NUM_SAMPS = length(tx_vec_air_A);
    
    transmit_A(cb_ii,:) = tx_vec_air_A;
    transmit_B(cb_ii,:) = tx_vec_air_B;
    
end
