close all;
clearvars;
import usrp.*;
%% Parameters
N_PA = [2 2];
N_CB = 16;
cb_gap = 256; % Number of samples to allow for codebook switching
Fs = 125e6; % 2.5 Msps sample rate
Fc = 2.4e9; % 2.4 GHz center frequency
TX_GAIN = 50;
RX_GAIN = 57;
FPA = 12e6; % PA controller FPGA clock frequency
PA_gain = [6 0; 3 6]; % Internal gain in the PA
% ^ IMPORTANT: this is where you configure how many PAs are transmitting
TX_PA = [5 6]; % Index for the tx PA
RX_PA = [7 8]; % Index for the rx PA
QUASI_OMNI_IDX=15;
NUM_RUNS=10;
samp_per_cb=2048;
MV_AVG_SPAN=20;

%% Build Payload

% Make sure we send enough samples to cycle through all the beams
txLength = (N_CB+2)*samp_per_cb;

Ts = 1/Fs;

t = (0:Ts:((txLength-1))*Ts).'; % Create time vector(Sample Frequency is Ts (Hz))

payload = repmat(exp(t*1i*2*pi*20e6), N_PA(1), 1);

%% Configure PA sweeping
pa_clk_count = round(samp_per_cb/Fs*FPA);

uartfh = serial('/dev/ttyUSB2','BaudRate', 115200);
fopen(uartfh);
for ii=1:N_PA(1)
    pa_ctl(QUASI_OMNI_IDX*ones(1,N_CB), PA_gain(1,ii), TX_PA(rx_idx), 1, pa_clk_count, uartfh);
end
for ii=1:N_PA(2)
    pa_ctl(0:(N_CB-1), PA_gain(2,ii), RX_PA(rx_idx), 0, pa_clk_count, uartfh);
end
fclose(uartfh);

%% Bring up USRP

usrph = USRPHandle(max(N_PA), Fs, Fc, RX_GAIN, TX_GAIN);
usrph.pa_arm_trigger();

%% Data Collection
best_rssi = zeros(1, NUM_RUNS);
collect_times = zeros(1, NUM_RUNS);

tic;
for collect_ii=1:NUM_RUNS
    rx_dat = usrph.txrx_data(payload);
    rx_1 = filter(ones(1,MV_AVG_SPAN)/MV_AVG_SPAN, 1, rx_dat(1,:));
    rx_2 = filter(ones(1,MV_AVG_SPAN)/MV_AVG_SPAN, 1, rx_dat(2,:));
    best_rssi(collect_ii) = max([rx_1; rx_2], [], 'all');
    collect_times(collect_ii) = toc;
end


%% Save the Data

