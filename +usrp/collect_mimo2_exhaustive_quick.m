% Exhaustive MIMO-OFDM estimation for 2x2 hybrid beamforming
% It is not advisable to use this exhaustive technique for larger antenna
% arrays because the amount of time required grows exponentially

clearvars

N_PA = 2;
N_CB = 64;
CB_GAP = 300;          % Number of samples to allow for codebook switching
Fs = 62.5e6;             % 125 Msps sample rate
Fc = 3e9;               % 3 GHz center frequency
TX_GAIN = 50;
RX_GAIN = 55;
PA_CTL_UART = '/dev/ttyUSB1';
TXA_PA = 1; % <<<<<<<< Probably need to change this
TXB_PA = 2;
RXA_PA = 3;
RXB_PA = 4;
PA_GAIN = 5;
FPA = 12e6; % PA controller FPGA clock frequency

% Set these using the sector sweep
REF_TXA_CB = 0;
REF_TXB_CB = 0;

% Data-related params
N_OFDM_SYMS             = 4;           % Number of OFDM symbols (must be even valued)
MOD_ORDER               = 2;           % BPSK
INTERP_RATE             = 1;           % Interpolation rate (must be 2)

REAL_EXHAUSTIVE         = 0; % Whether to generate a frighteningly large data set
DEBUG_PLOTS = 1;

%% tx data generation
% Generate an OFDM signal for each codebook entry
rftx.lofdm_mimo2_gen

% For exhaustive estimation, we'll have tx A sweep change each time, tx B
% beam change each time tx A completes a full sweep, and rx beams change
% each time tx B completes a full sweep.  This minimizes the effect of any
% timing offset between the rx and tx sweeps
txA_cbidx = repmat(1:N_CB, 1, N_CB*N_CB);
txB_cbidx = repmat(repelem(1:N_CB, 1, N_CB), 1, N_CB);
rx_cbidx = repelem(1:N_CB, 1, N_CB*N_CB);

% Simulate interference by giving transmit_A some data
if REAL_EXHAUSTIVE
    for ii=1:size(transmit_A, 1)
        sel_A = mod(ii-20, size(transmit_A, 1))+1;
        transmit_A(ii,352:end) = transmit_B(sel_A, 352:end);
    end
end

%% Data Collection

import usrp.USRPHandle;

% Initialize everything
usrph = USRPHandle(N_PA, Fs, Fc, TX_GAIN, RX_GAIN);
usrph.pa_arm_trigger();

uartfh = serial(PA_CTL_UART,'BaudRate', 115200);
fopen(uartfh);

xmit_unit_len = size(transmit_A,2)+2*CB_GAP;
pa_clk_count = round(xmit_unit_len/Fs*FPA);

assert(pa_clk_count^2 <= 2^16-1);

xmit_idx = floor((0:N_CB^2-1)*pa_clk_count*Fs/FPA) + CB_GAP;
xmit_all_A = zeros(1,xmit_idx(end)+xmit_unit_len);
xmit_all_B = zeros(1,xmit_idx(end)+xmit_unit_len);
txB_cbidx = mod((0:N_CB*N_CB-1), N_CB) + 1;
for ii=1:N_CB*N_CB
    %     txA_idx = mod(ii-1, N_CB) + 1;
    %     txB_idx = floor((ii-1)/N_CB) + 1;
    txA_idx = 1;
    txB_idx = mod(ii-1, N_CB) + 1;
    xmit_all_A((1:size(transmit_A,2))+xmit_idx(ii)) = transmit_A(txA_idx, :);
    xmit_all_B((1:size(transmit_A,2))+xmit_idx(ii)) = transmit_B(txB_idx, :);
end

if ~REAL_EXHAUSTIVE
    
    if 0
        figure; plot(abs(xmit_all_A(1:50000)));
        hold on; plot(abs(xmit_all_B(1:50000))); hold off;
    end
    
    
    tic
    pa_ctl(REF_TXA_CB, PA_GAIN, TXA_PA, 1, round(pa_clk_count), uartfh);
    pa_ctl(0:(N_CB-1), PA_GAIN, TXB_PA, 1, round(pa_clk_count), uartfh);
    pa_ctl(0:(N_CB-1), PA_GAIN, RXA_PA, 0, round(pa_clk_count)^2, uartfh);
    pa_ctl(0:(N_CB-1), PA_GAIN, RXB_PA, 0, round(pa_clk_count)^2, uartfh);
    rx_usrp_sw_B = usrph.txrx_data([xmit_all_A; xmit_all_B]);
    rx_sw_B_time = toc;
    
    tic
    pa_ctl(0:(N_CB-1), PA_GAIN, TXA_PA, 1, round(pa_clk_count), uartfh);
    pa_ctl(REF_TXB_CB, PA_GAIN, TXB_PA, 1, round(pa_clk_count), uartfh);
    pa_ctl(0:(N_CB-1), PA_GAIN, RXA_PA, 0, round(pa_clk_count)^2, uartfh);
    pa_ctl(0:(N_CB-1), PA_GAIN, RXB_PA, 0, round(pa_clk_count)^2, uartfh);
    rx_usrp_sw_A = usrph.txrx_data([xmit_all_B; xmit_all_A]);
    rx_sw_A_time = toc;
    
    fprintf('SW A: %f, SW B: %f, Total: %f', rx_sw_A_time, rx_sw_B_time, rx_sw_A_time+rx_sw_B_time);
else
    all_snrs_A = zeros(N_CB^2, N_CB*N_CB);
    all_snrs_B = zeros(size(all_snrs_A));
    all_capacities = zeros(size(all_snrs_A));
    all_capacities_mab = zeros(size(all_snrs_A));
    all_conditions = zeros(size(all_snrs_A));
    tic
    for ii=1:N_CB^2
        pa_ctl(0:(N_CB-1), PA_GAIN, TXA_PA, 1, round(pa_clk_count), uartfh);
        pa_ctl(0:(N_CB-1), PA_GAIN, TXB_PA, 1, round(pa_clk_count), uartfh);
        pa_ctl(ii-1, PA_GAIN, RXA_PA, 0, round(pa_clk_count)^2, uartfh);
        pa_ctl(ii-1, PA_GAIN, RXB_PA, 0, round(pa_clk_count)^2, uartfh);
        rx_dat = usrph.txrx_data([xmit_all_A; xmit_all_B]);
        lofdm_mimo2_proc
        all_snrs_A(ii,:) = snrs_rx_A;
        all_snrs_B(ii,:) = snrs_rx_B;
        all_capacities(ii,:) = capacity_mimo_ideal;
        all_capacities_mab(ii,:) = capacity_mab_ideal;
        all_conditions(ii,:) = channel_condition;
    end
    rx_sw_time = toc;
end

%% Save data
clear uartfh; clear usrph;
SRC_SCRIPT=mfilename;
[~,GIT_VER] = system('git rev-parse HEAD');
GIT_VER = GIT_VER(1:end-1);
% save([SRC_SCRIPT '_' datestr(datetime('now'), 'yymmdd-HHMMSS') '.mat']);

%% Diagnostic
rx_dat = rx_sw_A_time;
lofdm_mimo2_proc
figure(9);
subplot(2,2,1);
plot(abs(squeeze(rx_H_est(:,1,1,1:N_CB:end))));
title('H: 1->1');
subplot(2,2,2);
plot(abs(squeeze(rx_H_est(:,1,2,1:N_CB:end))));
title('H: 1->2');
subplot(2,2,3);
plot(abs(squeeze(rx_H_est(:,2,1,1:N_CB:end))));
title('H: 2->1');
subplot(2,2,4);
plot(abs(squeeze(rx_H_est(:,2,2,1:N_CB:end))));
title('H: 2->2');
% pause
% rx_dat = rx_sw_B_time;
% lofdm_mimo2_proc