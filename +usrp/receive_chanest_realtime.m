close all; clearvars;
import usrp.*;
%% Parameters
N_PA = [1 1];
Fs = 125e6; % 2.5 Msps sample rate
Fc = 1e9; % 2.4 GHz center frequency
FPA = 12e6; % PA controller FPGA clock frequency
PA_gain = 5; % Internal gain in the PA
RX_PA = 1; % Index for the tx PA
RX_CB_ENTRY = 1;
USE_AGC = 0;
DO_INTERPOLATION = 1;
RAND_SEED=5;
RX_LEN=round(0.05*125e6);

% OFDM params
N_OFDM_SYMS             = 32;
SC_IND_PILOTS           = [8 22 44 58];                           % Pilot subcarrier indices
SC_IND_DATA             = [2:7 9:21 23:27 39:43 45:57 59:64];     % Data subcarrier indices
N_SC                    = 64;                                     % Number of subcarriers
CP_LEN                  = 16;                                     % Cyclic prefix length
N_DATA_SYMS             = N_OFDM_SYMS * length(SC_IND_DATA);      % Number of data symbols (one per data-bearing subcarrier per OFDM symbol)

%% Generate training data

% OFDM data
[ltf, ltf_t, ltf_f] = l_ltf(0);
rng(RAND_SEED, 'twister');
tx_data = randi(2, 1, N_DATA_SYMS) - 1;
pilots = [1 1 -1 1].';
pilots_mat = repmat(pilots, 1, N_OFDM_SYMS);
tx_syms = arrayfun(mod_fcn_bpsk, tx_data);
tx_syms_mat = reshape(tx_syms, length(SC_IND_DATA), N_OFDM_SYMS);

% SC payload
cef = train.cef_80211ay(1,1);
cef = cef.*exp(1j*pi/2*(1:length(cef)));
cef_len = length(cef);
stf = train.getstf_80211ad();
stf_len = length(stf);

%% Receive data until we get a good sync
uartfh = serial('/dev/ttyUSB1','BaudRate', 115200);
fopen(uartfh);
pa_ctl(RX_CB_ENTRY, PA_gain, RX_PA, 1, pa_clk_count, uartfh);
fclose(uartfh);
usrp = USRPHandle(1, Fs, Fc, 10);
tx_vals = zeros(RX_LEN,1);

snr_vals = [];
rss_vals = [];
fprintf('No LTS 000\n');
for ii=1:999
    rx_vals = usrp.txrx_data(tx_vals);
    
    % Complex cross correlation of Rx waveform with time-domain LTS
    lts_corr = abs(conv(conj(fliplr(lts_t)), (raw_rx_dec)));
    
    % Skip early and late samples - avoids occasional false positives from pre-AGC samples
    lts_corr = lts_corr(32:end-32);
    
    % Find all correlation peaks
    lts_peaks = find(lts_corr(1:LTS_CORR_END) > LTS_CORR_THRESH*max(lts_corr(1:LTS_CORR_END)));
    
    % Select best candidate correlation peak as LTS-payload boundary
    [LTS1, LTS2] = meshgrid(lts_peaks,lts_peaks);
    [lts_second_peak_index,y] = find(LTS2-LTS1 == 4*length(lts_t));
    
    if(isempty(lts_second_peak_index))
        fprintf('\b\b\b\b%3d\n', ii);
        continue
    end
    
    % Set the sample indices of the payload symbols and preamble
    % The "+32" corresponds to the 32-sample cyclic prefix on the preamble LTS
    % The "-160" corresponds to the length of the preamble LTS (2.5 copies of 64-sample LTS)
    payload_ind = lts_peaks(max(lts_second_peak_index)) + 32;
    lts_ind = payload_ind-160;
    
    if lts_ind > round(0.9*RX_LEN)
        fprintf('\b\b\b\b%3d\n', ii);
        continue
    end
    
    %% Process packet
    
    %Extract LTS (not yet CFO corrected)
    rx_lts = raw_rx_dec(lts_ind : lts_ind+159);
    rx_lts1 = rx_lts(-64+-FFT_OFFSET + [97:160]);
    rx_lts2 = rx_lts(-FFT_OFFSET + [97:160]);
    
    %Calculate coarse CFO est
    rx_cfo_est_lts = mean(unwrap(angle(rx_lts2 .* conj(rx_lts1))));
    rx_cfo_est_lts = rx_cfo_est_lts/(2*pi*64);
    
    % Apply CFO correction to raw Rx waveform
    rx_cfo_corr_t = exp(-1i*2*pi*rx_cfo_est_lts*[0:length(raw_rx_dec)-1]);
    rx_dec_cfo_corr = raw_rx_dec .* rx_cfo_corr_t;
    
    % Re-extract LTS for channel estimate
    rx_lts = rx_dec_cfo_corr(lts_ind : lts_ind+159);
    rx_lts1 = rx_lts(-64+-FFT_OFFSET + [97:160]);
    rx_lts2 = rx_lts(-FFT_OFFSET + [97:160]);
    
    rx_lts1_f = fft(rx_lts1);
    rx_lts2_f = fft(rx_lts2);
    rx_H_est = lts_f .* (rx_lts1_f + rx_lts2_f)/2;
    
    %% Rx payload processing
    
    % Extract the payload samples (integral number of OFDM symbols following preamble)
    payload_vec = rx_dec_cfo_corr(payload_ind : payload_ind+N_OFDM_SYMS*(N_SC+CP_LEN)-1);
    payload_mat = reshape(payload_vec, (N_SC+CP_LEN), N_OFDM_SYMS);
    
    % Remove the cyclic prefix, keeping FFT_OFFSET samples of CP (on average)
    payload_mat_noCP = payload_mat(CP_LEN-FFT_OFFSET+[1:N_SC], :);
    
    % Take the FFT
    syms_f_mat = fft(payload_mat_noCP, N_SC, 1);
    
    % Equalize (zero-forcing, just divide by complex chan estimates)
    syms_eq_mat = syms_f_mat ./ repmat(rx_H_est.', 1, N_OFDM_SYMS);
    
    % Extract the pilot tones and "equalize" them by their nominal Tx values
    pilots_f_mat = syms_eq_mat(SC_IND_PILOTS, :);
    pilots_f_mat_comp = pilots_f_mat.*pilots_mat;
    
    % Calculate the phases of every Rx pilot tone
    pilot_phases = unwrap(angle(fftshift(pilots_f_mat_comp,1)), [], 1);
    
    % Calculate slope of pilot tone phases vs frequency in each OFDM symbol
    pilot_spacing_mat = repmat(mod(diff(fftshift(SC_IND_PILOTS)),64).', 1, N_OFDM_SYMS);
    pilot_slope_mat = mean(diff(pilot_phases) ./ pilot_spacing_mat);
    
    % Calculate the SFO correction phases for each OFDM symbol
    pilot_phase_sfo_corr = fftshift((-32:31).' * pilot_slope_mat, 1);
    pilot_phase_corr = exp(-1i*(pilot_phase_sfo_corr));
    
    % Apply the pilot phase correction per symbol
    syms_eq_mat = syms_eq_mat .* pilot_phase_corr;
    
    % Extract the pilots and calculate per-symbol phase error
    pilots_f_mat = syms_eq_mat(SC_IND_PILOTS, :);
    pilots_f_mat_comp = pilots_f_mat.*pilots_mat;
    pilot_phase_err = angle(mean(pilots_f_mat_comp));
    
    pilot_phase_err_corr = repmat(pilot_phase_err, N_SC, 1);
    pilot_phase_corr = exp(-1i*(pilot_phase_err_corr));
    
    % Apply the pilot phase correction per symbol
    syms_eq_pc_mat = syms_eq_mat .* pilot_phase_corr;
    payload_syms_mat = syms_eq_pc_mat(SC_IND_DATA, :);
    
    %% Results
    
    % SNR
    evm_mat = abs(payload_syms_mat - tx_syms_mat).^2;
    aevms = mean(evm_mat(:));
    snr = 10*log10(1./aevms);
    snr_vals = [snr_vals snr];
    fprintf('SNR: %d dB\n', snr);
    
    % RSSI, I think?
    rssi = mag2db(mean(rx_H_est));
    rss_vals = [rss_vals rssi];
    fprintf('RSS: %d dB\n', rssi);
    
    fprintf('No LTS 000\n');
end

%% Averages
figure(1); histogram(rss_vals); title('RSSI Values Histogram');
figure(2); histogram(snr_vals); title('SNR Values Histogram');

fprintf('Average SNR: %d dB\n', mean(snr_vals));
fprintf('Average RSSI: %d dB\n', mean(rss_vals));