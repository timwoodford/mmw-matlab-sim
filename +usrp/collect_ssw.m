close all; clearvars;
%% Parameters
N_PA = [1 1];
N_CB = 65;
cb_gap = 512; % Number of samples to allow for codebook switching
Fs = 125e6; % 2.5 Msps sample rate
Fc = 1e9; % 2.4 GHz center frequency
FPA = 12e6; % PA controller FPGA clock frequency
PA_gain = 5; % Internal gain in the PA
TX_PA = 1; % Index for the tx PA
RX_PA = 2; % Index for the rx PA
USE_AGC = 0;

%% Generate Training Data

cef = train.cef_80211ay(1,1);
cef = cef.*exp(1j*pi/2*(1:length(cef)));
cef_len = length(cef);
stf = train.getstf_80211ad();
stf_len = length(stf);
xmit_unit = [zeros(1, cb_gap) stf cef zeros(1,cb_gap)];
pa_clk_count = round(length(xmit_unit)/Fs*FPA);
xmit_idx = floor((0:N_CB-1)*pa_clk_count*Fs/FPA);
xmit_all = zeros(1,xmit_idx(end)+length(xmit_unit));
for ii=1:N_CB
    xmit_all((1:length(xmit_unit))+xmit_idx(ii)) = xmit_unit;
end

%% Data collection from USRP
uartfh = serial('/dev/ttyUSB1','BaudRate', 115200);
fopen(uartfh);
pa_ctl(0:64, PA_gain, TX_PA, 1, pa_clk_count, uartfh);
CB_RX = 0:64;
pa_ctl(0, PA_gain, RX_PA, 0, pa_clk_count*65, uartfh);
CB_TX = zeros(1,65);
fclose(uartfh);
usrp = USRPHandle(1, Fs, Fc, 10);
usrp.pa_arm_trigger();
rx_matrix = usrp.txrx_data(xmit_all.');

%% Save data
clear uartfh; clear usrp;
SRC_SCRIPT=mfilename;
save([SRC_SCRIPT '_' datestr(datetime('now'), 'yymmdd-HHMMSS') '.mat']);