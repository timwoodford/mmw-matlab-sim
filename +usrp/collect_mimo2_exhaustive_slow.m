% Exhaustive MIMO-OFDM estimation for 2x2 hybrid beamforming
% It is not advisable to use this exhaustive technique for larger antenna
% arrays because the amount of time required grows exponentially

clearvars

CODEBOOK_SELECTOR=1;
if CODEBOOK_SELECTOR==1
    N_CB=16;
    CB_START=0;
elseif CODEBOOK_SELECTOR==2
    N_CB=8;
    CB_START=16;
else
    error('Invalid codebook selection');
end

N_PA = 2;
CB_GAP = 300;          % Number of samples to allow for codebook switching
Fs = 62.5e6;             % 125 Msps sample rate
Fc = 2.4e9;               % 3 GHz center frequency
TX_GAIN = 50;
RX_GAIN = 61;
PA_CTL_UART = '/dev/ttyUSB2';
TXA_PA = 5; % <<<<<<<< Probably need to change this
TXB_PA = 6;
RXA_PA = 7;
RXB_PA = 8;
PA_GAIN = 5;
FPA = 12e6; % PA controller FPGA clock frequency

% Set these using the sector sweep
REF_TXA_CB = 0;
REF_TXB_CB = 0;

% Data-related params
N_OFDM_SYMS             = 32;          % Number of OFDM symbols (must be even valued)
MOD_ORDER               = 2;           % BPSK
INTERP_RATE             = 1;           % Interpolation rate (must be 2)

REAL_EXHAUSTIVE         = 0; % Whether to generate a frighteningly large data set
DEBUG_PLOTS = 1;

%% tx data generation
% Generate an OFDM signal for each codebook entry
rftx.lofdm_mimo2_gen_slow

% For exhaustive estimation, we'll have tx A sweep change each time, tx B
% beam change each time tx A completes a full sweep, and rx beams change
% each time tx B completes a full sweep.  This minimizes the effect of any
% timing offset between the rx and tx sweeps
txA_cbidx = repmat(1:N_CB, 1, N_CB*N_CB);
txB_cbidx = repmat(repelem(1:N_CB, 1, N_CB), 1, N_CB);
rx_cbidx = repelem(1:N_CB, 1, N_CB*N_CB);

%% Data Collection

import usrp.USRPHandle;

% Initialize everything
usrph = USRPHandle(N_PA, Fs, Fc, TX_GAIN, RX_GAIN);
usrph.pa_arm_trigger();

uartfh = serial(PA_CTL_UART,'BaudRate', 115200);

xmit_unit_len = size(transmit_A,2)+2*CB_GAP;
pa_clk_count = round(xmit_unit_len/Fs*FPA);

assert(pa_clk_count*N_CB <= 2^16-1);

xmit_idx = floor((0:N_CB^2-1)*pa_clk_count*Fs/FPA) + CB_GAP;
xmit_all_A = zeros(1,xmit_idx(end)+xmit_unit_len);
xmit_all_B = zeros(1,xmit_idx(end)+xmit_unit_len);
for ii=1:N_CB*N_CB
    txA_idx = mod(ii-1, N_CB) + 1;
    txB_idx = floor((ii-1)/N_CB) + 1;
    xmit_all_A((1:size(transmit_A,2))+xmit_idx(ii)) = transmit_A(txA_idx, :);
    xmit_all_B((1:size(transmit_A,2))+xmit_idx(ii)) = transmit_B(txB_idx, :);
end

all_capacities_zf = zeros(N_CB^2, N_CB*N_CB);
all_chan_conditions = zeros(size(all_capacities_zf));
all_capacities_mmse = zeros(size(all_capacities_zf));
all_capacities_mab = zeros(size(all_capacities_zf));
all_snrs = zeros(size(all_capacities_zf));
all_channels = zeros(N_CB^2, N_CB^2, 2, 2, 64);

fopen(uartfh);
tic
for ii=1:N_CB^2
    ii
%     fopen(uartfh);
    pa_ctl(0:(N_CB-1)+CB_START, PA_GAIN, TXA_PA, 1, round(pa_clk_count), uartfh);
    pa_ctl(0:(N_CB-1)+CB_START, PA_GAIN, TXB_PA, 1, N_CB*round(pa_clk_count), uartfh);
    pa_ctl(mod(ii-1, N_CB)+CB_START, PA_GAIN, RXA_PA, 0, round(pa_clk_count), uartfh);
    pa_ctl(floor((ii-1)/N_CB)+CB_START, PA_GAIN, RXB_PA, 0, round(pa_clk_count), uartfh);
%     fclose(uartfh);
    rx_data = usrph.txrx_data([xmit_all_A; xmit_all_B]);
    rfrx.lofdm_mimo2_proc3
    all_raw_data(ii,:,:) = rx_data;
    all_chan_conditions(ii,:) = channel_condition;
    all_capacities_zf(ii,:) = capacity_multiplexing_zf;
    all_capacities_mab(ii,:) = capacity_beamf;
    all_capacities_mmse(ii,:) = capacity_multiplexing_mmse;
    all_snrs(ii,:) = snr_est_out;
    all_channels(ii,:,:,:,:) = rx_H_est;
    toc
end
rx_sw_time = toc;
fclose(uartfh);

%% Save data
clear uartfh; clear usrph;
SRC_SCRIPT=mfilename;
[~,GIT_VER] = system('git rev-parse HEAD');
GIT_VER = GIT_VER(1:end-1);
save([SRC_SCRIPT '_' datestr(datetime('now'), 'yymmdd-HHMMSS') '.mat']);
