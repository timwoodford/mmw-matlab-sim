% close all; clearvars;
%% Parameters
N_PA = 1;
N_CB = 65;
cb_gap = 513; % Number of samples to allow for codebook switching
Fs = 125e6; % 2.5 Msps sample rate
Fc = 1e9; % 2.4 GHz center frequency
FPA = 12e6; % PA controller FPGA clock frequency
PA_gain = 5; % Internal gain in the PA
TX_PA = 1; % Index for the tx PA
RX_PA = 2; % Index for the rx PA
DO_SIM = 1;
DO_OTA = 0;
DETECT_THRESH = 0.8;
USE_AGC = 0;

%% Generate Training Data

cef = train.cef_80211ay(1,1);
cef = cef.*exp(1j*pi/2*(1:length(cef)));
cef_len = length(cef);
stf = train.getstf_80211ad();
stf_len = length(stf);
xmit_unit = [zeros(1, cb_gap) stf cef zeros(1,cb_gap)];
pa_clk_count = round(length(xmit_unit)/Fs*FPA);
xmit_idx = floor((0:N_CB-1)*pa_clk_count*Fs/FPA);
% xmit_idx = (0:N_CB-1)*length(xmit_unit);
xmit_all = zeros(1,xmit_idx(end)+length(xmit_unit));
for ii=1:N_CB
    xmit_all((1:length(xmit_unit))+xmit_idx(ii)) = xmit_unit;
end

%% Data Collection
import usrp.USRPHandle;

if DO_SIM
    channel = 0.3*exp(-0.3j);
    cfo = 100e3;
    rx_matrix = [zeros(1,20) channel*xmit_all.*exp(2j*pi*cfo/Fs*(1:length(xmit_all)))];
    rx_matrix = rx_matrix + 0.03*randn(size(rx_matrix));
elseif DO_OTA
    uartfh = serial('/dev/ttyUSB1','BaudRate', 115200);
    fopen(uartfh);
    pa_ctl(0:64, PA_gain, TX_PA, 1, pa_clk_count, uartfh);
    pa_ctl(0:64, PA_gain, RX_PA, 0, pa_clk_count*65, uartfh);
    fclose(uartfh);
    usrp = USRPHandle(1, Fs, Fc, 10);
    if USE_AGC
        usrp.set_rx_gain(0,1);
    end
    usrp.pa_arm_trigger();
    rx_matrix = usrp.txrx_data(xmit_all.');
end

% Zero-padding to avoid processing errors on last codebook index
rx_matrix = [rx_matrix zeros(1,cb_gap)];

%% Data Processing
snrs=zeros(1,N_CB);
rss=zeros(1,N_CB);
cfos=zeros(1,N_CB);
cef_start_vals=zeros(1,N_CB);
% Note that we can't get phases at this stage because we have to
% re-estimate the CFO for each codebook entry.  If you need relative phase,
% we can probably use the results from here to estimate CFO, then do
% global CFO correction
for cb_idx=0:(N_CB-1)
%     this_rx = rx_matrix((1:(length(xmit_unit)+cb_gap))+cb_idx*length(xmit_unit));
    this_rx = rx_matrix((1:(length(xmit_unit)))+xmit_idx(cb_idx+1));
    
    % Step 1: STF coarse timesync
   
    [stf_start, stf_end] = rfrx.stf_coarse_sync(this_rx, stf(1:128), DETECT_THRESH);
    
    if stf_end - stf_start < 1024
        fprintf("Couln't find STFs for CFO estimation\n");
        continue
    end
    
    % Step 2: estimate CFO
    % Reduce stf_end by 128 because the last Ga may be part of the CEF
    % This will cause a phase offset because the STF and CEF are pi/2
    % rotated separately
    cfo_est = rfrx.stf_cfo_estimate(this_rx, stf_start, stf_end-128);
    cfos(cb_idx+1) = cfo_est;
    this_rx_cfo = this_rx.*exp(-1j*cfo_est*(1:length(this_rx)));
    
    % Step 3: Re-estimate timing offset
    xc_stf = xcorr(this_rx_cfo,stf(1:128));
    xc_stf = xc_stf(length(this_rx):end);
    xc_gb = xcorr(this_rx_cfo, cef(129:256));
    xc_gb = xc_gb(length(this_rx):end);
    [~, gb_idx] = maxk(xc_gb((cb_gap+length(stf)):end),4);
    gb_idx = gb_idx + cb_gap + length(stf);
    if ~all(diff(sort(gb_idx))==256)
        fprintf('CEF not found\n');
        cef_start = 0;
    else
        cef_start = min(gb_idx)-128;
    end
    
    
    % First pass: if we haven't successfully found the CEF, set SNR=0
    if cef_start + length(cef) > length(this_rx) || cef_start < length(stf)
        rss(cb_idx+1)=0;
        snr(cb_idx+1)=0;
        cef_start_vals(cb_idx+1) = NaN;
        cfos(cb_idx+1)=NaN;
        continue
    end
    
    cef_start_vals(cb_idx+1) = cef_start;
    
    % Step 4: SC channel estimation
    rx_cef = this_rx_cfo((1:length(cef))+cef_start-1);
    sc_chanest = (rx_cef*cef')/(cef*cef');
    
    % Step 5: Comparison and SNR estimation
    evm = abs(rx_cef./sc_chanest - cef).^2;
    aevm = mean(evm(:));
    snr = 10*log10(1./aevm);
    
    rss(cb_idx+1)=abs(sc_chanest);
    snrs(cb_idx+1)=snr;
end

%% Find relative phases
% This requires doing global CFO correction
% Find CFO from highest-SNR
rss_nan = rss((~isnan(rss))&(~isnan(cfos)));
cfo_nan = cfos((~isnan(rss))&(~isnan(cfos)));
cfo_est = rss_nan*cfo_nan'/sum(rss_nan);
rx_cfo_corr = rx_matrix.*exp((1:length(rx_matrix))*cfo_est*-1j);
sc_ces = zeros(1,N_CB);
for cb_idx=0:(N_CB-1)
    this_rx = rx_cfo_corr((1:(length(xmit_unit)))+xmit_idx(cb_idx+1));
    % Step 1: STF coarse timesync
    [stf_start, stf_end] = rfrx.stf_coarse_sync(this_rx, stf(1:128), DETECT_THRESH);
    assert(stf_end - stf_start <= length(stf));
    
    % Step 2: estimate CFO
    cfo_est = rfrx.stf_cfo_estimate(this_rx, stf_start, stf_end);
    cfos2(cb_idx+1) = cfo_est*Fs/2/pi;
%     this_rx_cfo = this_rx.*exp(-1j*cfo_est*(1:length(this_rx)));
    
    % Step 3: Find beginning of CEF
%     xc_gb = xcorr(this_rx, cef(129:256));
%     xc_gb = xc_gb(length(this_rx):end);
%     [~, gb_idx] = maxk(xc_gb((cb_gap+length(stf)):end),4);
%     gb_idx = gb_idx + cb_gap + length(stf);
%     if ~all(diff(sort(gb_idx))==256)
%         cef_start_2 =2893;
%     else
%         cef_start_2 = min(gb_idx)-128;
%     end
    [cef_start, cir] = rfrx.cef_fine_sync(this_rx(stf_len+cb_gap:end), ...
                           stf(1:128), cef(129:256), mod(stf_end, 128));
    cef_start = cef_start + stf_len+cb_gap;
    cef_starts_2(cb_idx+1)=cef_start;
    figure(3); plot(abs(cir)); title('CIR'); ylim([0 100]);
%     figure; plot(abs(cir)); title('CIR');
%     assert(cef_start_2 == cef_start);
    
    
    if cef_start + length(cef) > length(this_rx) || cef_start < length(stf)
        error('Indexes are bad');
    end

    
    % Step 4: SC channel estimation
    rx_cef = this_rx((1:length(cef))+cef_start-2);
    sc_chanest = (rx_cef*cef')/(cef*cef');
    rx_cef_a = abs(rx_cef);
    SN = (1/2)*((mean(abs(rx_cef_a)))^2);
    N = mean(sum(rx_cef_a.^2)) - (sum(abs(rx_cef_a))^2/(length(rx_cef_a)*(length(rx_cef_a)-1)));
%     snr = mag2db((mean(rx_cef_a.^2)-var(rx_cef_a))/var(rx_cef_a));
    snr = mag2db((SN - N)/N);
    
    figure(2);
    plot(rx_cef/sc_chanest,'.');
    hold on;
    plot(cef,'o');
    hold off;
    title('CEF Constellation (SC Correction Only)');
    
    distFig('Rows',2,'Columns',3)
    snrs2(cb_idx+1) = snr;
    rss2(cb_idx+1) = abs(sc_chanest);
    rss3(cb_idx+1) = mean(abs(rx_cef).^2);
    sc_ces(cb_idx+1) = sc_chanest;
    
    [ofdm_est, snr] = rfrx.cef_est_ofdm(rx_cef, cef);
    snrs3(cb_idx+1) = snr;
    figure(5); hold on; plot(abs(ofdm_est));
    pause(0.1);
end

figure(4);
subplot(2,1,1);
bar(snrs3);
title('Per-beam SNR');
subplot(2,1,2);
bar(rss3);
title('Per-beam RSS');


figure(7);
subplot(2,1,1);
bar(snrs3);
title('Per-beam SNR');
subplot(2,1,2);
bar(snrs2);
title('Per-beam SNR (new method)');
%% OFDM Estimation

figure(5);
hold off; axis tight;
title('Estimated Frequency Response');
% ylim([0 max(max(abs(ofdm_ce)))*1.1]);

distFig('Rows',2,'Columns',3)
