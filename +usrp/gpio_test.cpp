#include<chrono>
#include<thread>
#include<uhd/usrp/multi_usrp.hpp>

#define TRIGGER_BIT (0x100 << 0)
#define GPIO_PANEL "FP0"

int main(int argc, char *argv[]) {
    //
    std::string addr = "";
    uhd::usrp::multi_usrp::sptr usrp = uhd::usrp::multi_usrp::make(addr);
    usrp->set_gpio_attr(GPIO_PANEL, "CTRL", 0, TRIGGER_BIT);
    usrp->set_gpio_attr(GPIO_PANEL, "DDR", TRIGGER_BIT, TRIGGER_BIT);
    while (1) {
        usrp->set_gpio_attr(GPIO_PANEL, "OUT", TRIGGER_BIT, TRIGGER_BIT);
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        usrp->set_gpio_attr(GPIO_PANEL, "OUT", 0, TRIGGER_BIT);
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }
    return 0;
}