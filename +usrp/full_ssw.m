close all;
clearvars;
import usrp.*;
%% Parameters
N_PA = [2 2];
N_CB = 64;
cb_gap = 512; % Number of samples to allow for codebook switching
Fs = 125e6; % 2.5 Msps sample rate
Fc = 2.4e9; % 2.4 GHz center frequency
TX_GAIN = 50;
RX_GAIN = 57;
FPA = 12e6; % PA controller FPGA clock frequency
PA_gain = 5; % Internal gain in the PA
TX_PA = [5 6]; % Index for the tx PA
RX_PA = [7 8]; % Index for the rx PA
QUASI_OMNI_IDX=0;
USE_AGC = 0;
DO_INTERPOLATION = 1;
LOOP_MODE = 1; % Loop mode displays data but doesn't save it

% OFDM params
N_OFDM_SYMS             = 32;
SC_IND_PILOTS           = [8 22 44 58];                           % Pilot subcarrier indices
SC_IND_DATA             = [2:7 9:21 23:27 39:43 45:57 59:64];     % Data subcarrier indices
N_SC                    = 64;                                     % Number of subcarriers
CP_LEN                  = 16;                                     % Cyclic prefix length
N_DATA_SYMS             = N_OFDM_SYMS * length(SC_IND_DATA);      % Number of data symbols (one per data-bearing subcarrier per OFDM symbol)

%% Generate training data

% OFDM data
ltf_f = [0 1 -1 -1 1 1 -1 1 -1 1 -1 -1 -1 -1 -1 1 1 -1 -1 1 -1 1 -1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 1 1 -1 -1 1 1 -1 1 -1 1 1 1 1 1 1 -1 -1 1 1 -1 1 -1 1 1 1 1];
ltf_t = ifft(ltf_f, 64);
ltf = [ltf_t(33:64) ltf_t ltf_t ltf_t ltf_t ltf_t];
tx_data = randi(2, 1, N_DATA_SYMS) - 1;
modvec_bpsk   =  (1/sqrt(2))  .* [-1 1];
mod_fcn_bpsk  = @(x) complex(modvec_bpsk(1+x),0);
tx_syms = arrayfun(mod_fcn_bpsk, tx_data);
tx_syms_mat = reshape(tx_syms, length(SC_IND_DATA), N_OFDM_SYMS);
pilots = [1 1 -1 1].';
pilots_mat = repmat(pilots, 1, N_OFDM_SYMS);
ifft_in_mat = zeros(N_SC, N_OFDM_SYMS);
ifft_in_mat(SC_IND_DATA, :)   = tx_syms_mat;
ifft_in_mat(SC_IND_PILOTS, :) = pilots_mat;
tx_payload_mat = ifft(ifft_in_mat, N_SC, 1);
tx_cp = tx_payload_mat((end-CP_LEN+1 : end), :);
tx_payload_mat = [tx_cp; tx_payload_mat];
tx_ofdm_payload = [ltf reshape(tx_payload_mat, 1, numel(tx_payload_mat))];
tx_ofdm_payload = tx_ofdm_payload./max(abs(tx_ofdm_payload));

%% Optional: Interpolation
if DO_INTERPOLATION
    interp_filt2 = zeros(1,43);
    interp_filt2([1 3 5 7 9 11 13 15 17 19 21]) = [12 -32 72 -140 252 -422 682 -1086 1778 -3284 10364];
    interp_filt2([23 25 27 29 31 33 35 37 39 41 43]) = interp_filt2(fliplr([1 3 5 7 9 11 13 15 17 19 21]));
    interp_filt2(22) = 16384;
    interp_filt2 = interp_filt2./max(abs(interp_filt2));
    
    tx_vec_2x = zeros(1, 2*numel(tx_ofdm_payload));
    tx_vec_2x(1:2:end) = tx_ofdm_payload;
    tx_vec_2x = [zeros(1,32) tx_vec_2x zeros(1,32)];
    tx_vec_air = filter(interp_filt2, 1, tx_vec_2x);
    
    tx_ofdm_payload = tx_vec_air / max(abs(tx_vec_air));
else
    tx_sc_payload = [stf cef];
    tx_ofdm_payload = tx_ofdm_payload / max(abs(tx_ofdm_payload));
    tx_sc_payload = tx_sc_payload / max(abs(tx_sc_payload));
end

%% Packaging
xmit_unit = [zeros(1, cb_gap) tx_ofdm_payload zeros(1,cb_gap)];
pa_clk_count = round(length(xmit_unit)/Fs*FPA);
if DO_INTERPOLATION
    pa_clk_count = pa_clk_count * 2;
end
xmit_idx = floor((0:N_CB-1)*pa_clk_count*Fs/FPA);
xmit_all = zeros(1,xmit_idx(end)+length(xmit_unit));
for ii=1:N_CB
    xmit_all((1:length(xmit_unit))+xmit_idx(ii)) = xmit_unit;
end

%% Run data collection
uartfh = serial('/dev/ttyUSB2','BaudRate', 115200);

usrph = USRPHandle(max(N_PA), Fs, Fc, RX_GAIN, TX_GAIN);
usrph.pa_arm_trigger();

% tx sweeps
tx_sw_snr = zeros(N_PA(1), N_PA(2), N_CB);
tx_sw_rss = zeros(N_PA(1), N_PA(2), N_CB);
for tx_idx = 1:N_PA(1)
    fopen(uartfh);
    pa_ctl(0:(N_CB-1), PA_gain, TX_PA(tx_idx), 1, pa_clk_count, uartfh);
    for rx_idx=1:N_PA(2)
        pa_ctl(QUASI_OMNI_IDX*ones(1,N_CB), PA_gain, RX_PA(rx_idx), 0, pa_clk_count, uartfh);
    end
    fclose(uartfh);
    
    xmit_data = zeros(N_PA(1), length(xmit_all));
    xmit_data(tx_idx, :) = xmit_all;
    
    rx_matrix_all = usrph.txrx_data(xmit_data);
    for rx_idx = 1:N_PA(2)
        rx_matrix = rx_matrix_all(rx_idx,:);
        rfrx.proc_ssw_xcheck;
        tx_sw_snr(tx_idx,rx_idx,:) = snrs;
        tx_sw_rss(tx_idx,rx_idx,:) = rssi;
    end
end

% rx sweeps
rx_sw_snr = zeros(N_PA(1), N_PA(2), N_CB);
rx_sw_rss = zeros(N_PA(1), N_PA(2), N_CB);
for rx_idx = 1:N_PA(2)
    fopen(uartfh);
    pa_ctl(0:(N_CB-1), PA_gain, RX_PA(rx_idx), 0, pa_clk_count, uartfh);
    for tx_idx=1:N_PA(2)
        pa_ctl(QUASI_OMNI_IDX*ones(1,N_CB), PA_gain, TX_PA(tx_idx), 1, pa_clk_count, uartfh);
    end
    fclose(uartfh);
    
    for tx_idx = 1:N_PA(1)
        xmit_data = zeros(N_PA(2), length(xmit_all));
        xmit_data(tx_idx, :) = xmit_all;
        
        rx_matrix = usrph.txrx_data(xmit_data);
        rx_matrix = rx_matrix(rx_idx, :);
        rfrx.proc_ssw_xcheck;
        rx_sw_snr(tx_idx, rx_idx,:) = snrs;
        rx_sw_rss(tx_idx, rx_idx,:) = rssi;
    end
end

%% Save data
clear uartfh usrph mod_fcn_bpsk
SRC_SCRIPT=mfilename;
save([SRC_SCRIPT '_' datestr(datetime('now'), 'yymmdd-HHMMSS') '.mat'], '-regexp', '\w_sw_\w');
