close all; clearvars;
import usrp.*;
%% Parameters
N_PA = [1 1];
Fs = 125e6; % 2.5 Msps sample rate
Fc = 1e9; % 2.4 GHz center frequency
FPA = 12e6; % PA controller FPGA clock frequency
PA_gain = 5; % Internal gain in the PA
TX_PA = 1; % Index for the tx PA
TX_CB_ENTRY = 1;
USE_AGC = 0;
DO_INTERPOLATION = 1;
RAND_SEED=5;

% OFDM params
N_OFDM_SYMS             = 32;
SC_IND_PILOTS           = [8 22 44 58];                           % Pilot subcarrier indices
SC_IND_DATA             = [2:7 9:21 23:27 39:43 45:57 59:64];     % Data subcarrier indices
N_SC                    = 64;                                     % Number of subcarriers
CP_LEN                  = 16;                                     % Cyclic prefix length
N_DATA_SYMS             = N_OFDM_SYMS * length(SC_IND_DATA);      % Number of data symbols (one per data-bearing subcarrier per OFDM symbol)

%% Generate training data

% OFDM data
[ltf, ltf_t, ltf_f] = l_ltf(0);
rng(RAND_SEED, 'twister');
tx_data = randi(2, 1, N_DATA_SYMS) - 1;
modvec_bpsk   =  (1/sqrt(2))  .* [-1 1];
mod_fcn_bpsk  = @(x) complex(modvec_bpsk(1+x),0);
tx_syms = arrayfun(mod_fcn_bpsk, tx_data);
tx_syms_mat = reshape(tx_syms, length(SC_IND_DATA), N_OFDM_SYMS);
pilots = [1 1 -1 1].';
pilots_mat = repmat(pilots, 1, N_OFDM_SYMS);
ifft_in_mat = zeros(N_SC, N_OFDM_SYMS);
ifft_in_mat(SC_IND_DATA, :)   = tx_syms_mat;
ifft_in_mat(SC_IND_PILOTS, :) = pilots_mat;
tx_payload_mat = ifft(ifft_in_mat, N_SC, 1);
tx_cp = tx_payload_mat((end-CP_LEN+1 : end), :);
tx_payload_mat = [tx_cp; tx_payload_mat];
tx_ofdm_payload = [ltf reshape(tx_payload_mat, 1, numel(tx_payload_mat))];
tx_ofdm_payload = tx_ofdm_payload./max(abs(tx_ofdm_payload));

% SC payload
cef = train.cef_80211ay(1,1);
cef = cef.*exp(1j*pi/2*(1:length(cef)));
cef_len = length(cef);
stf = train.getstf_80211ad();
stf_len = length(stf);

%% Optional: Interpolation
if DO_INTERPOLATION
    interp_filt2 = zeros(1,43);
    interp_filt2([1 3 5 7 9 11 13 15 17 19 21]) = [12 -32 72 -140 252 -422 682 -1086 1778 -3284 10364];
    interp_filt2([23 25 27 29 31 33 35 37 39 41 43]) = interp_filt2(fliplr([1 3 5 7 9 11 13 15 17 19 21]));
    interp_filt2(22) = 16384;
    interp_filt2 = interp_filt2./max(abs(interp_filt2));
    
    tx_vec_2x = zeros(1, 2*numel(tx_ofdm_payload));
    tx_vec_2x(1:2:end) = tx_ofdm_payload;
    tx_vec_2x = [zeros(1,32) tx_vec_2x zeros(1,32)];
    tx_vec_air = filter(interp_filt2, 1, tx_vec_2x);
    
    tx_ofdm_payload = tx_vec_air / max(abs(tx_vec_air));
    
    tx_vec_2x = zeros(1, 2*numel([stf cef]));
    tx_vec_2x(1:2:end) = [stf cef];
    tx_vec_2x = [zeros(1,32) tx_vec_2x zeros(1,32)];
    tx_vec_air = filter(interp_filt2, 1, tx_vec_2x);
    
    tx_sc_payload = tx_vec_air / max(abs(tx_vec_air));
else
    tx_sc_payload = [stf cef];
    tx_ofdm_payload = tx_ofdm_payload / max(abs(tx_ofdm_payload));
    tx_sc_payload = tx_sc_payload / max(abs(tx_sc_payload));
end

%% Packaging
xmit_unit = [zeros(1, 128) tx_ofdm_payload zeros(1,8) tx_sc_payload zeros(1,128)];
xmit_vals = repmat(xmit_unit,1,16);

%% Repeated transmission
uartfh = serial('/dev/ttyUSB1','BaudRate', 115200);
fopen(uartfh);
pa_ctl(TX_CB_ENTRY, PA_gain, TX_PA, 1, pa_clk_count, uartfh);
fclose(uartfh);
usrp = USRPHandle(1, Fs, Fc, 10);
fprintf('Transmission time: 0.0000\n');
while true
    tic
    usrp.txrx_data(xmit_vals.');
    fprintf('\b\b\b\b\b\b\b%1.4f\n', toc);
end