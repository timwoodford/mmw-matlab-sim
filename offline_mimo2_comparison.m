% Offline comparison of MIMO methods for exhaustive collection
% clearvars
% load('data/mimo/los/collect_mimo2_exhaustive_slow_190725-151408.mat', '-regexp', '^all_(.*)')
% load('data/mimo/abcxyz/full_ssw_190000-123456.mat');

all_capacities_mab(isnan(all_capacities_mab))=0;
all_capacities_zf(isnan(all_capacities_zf))=0;
all_capacities_mmse(isnan(all_capacities_mmse))=0;

%% Data preprocessing

USE_MAB_APPROX=1;

if USE_MAB_APPROX
    % For now, we can probably use average zf capacity as a reasonable proxy,
    % since we may not have a working quasi-omni antenna
    
    % Recall that rx A and tx A are swept "quickly", and rx B and tx B are
    % changed slowly
    
    % First index for capacity matrices is rx, second index is tx
    
    N_CB = sqrt(size(all_capacities_mab,2));
    % First idx is tx A ("fast"), second is tx B ("slow")
    tx_means = reshape(max(all_snrs,[],1),N_CB,N_CB);
    txA_ssw = mean(tx_means, 2).';
    txB_ssw = mean(tx_means, 1);
    tx_ssw = [txA_ssw; txB_ssw];
    % First idx is rx A ("fast"), second is rx B ("slow")
    rx_means = reshape(max(all_snrs,[],2),N_CB,N_CB);
    rxA_ssw = mean(rx_means, 2).';
    rxB_ssw = mean(rx_means, 1);
    rx_ssw = [rxA_ssw; rxB_ssw];
else
    % There are a number of options we can use here: the most obvious 
    % (used here) is to take the mean from both quasi-omni elements
    tx_ssw = squeeze(mean(tx_sw_snr, 2));
    rx_ssw = squeeze(mean(rx_sw_snr, 1));
end

% Reshape capacity data such that we have (rx 1,2,..., tx 1,2,...)
shaped_cap_mab = reshape(all_capacities_mab, N_CB, N_CB, N_CB, N_CB);
shaped_cap_zf = reshape(all_capacities_zf, N_CB, N_CB, N_CB, N_CB);
shaped_chan_c = reshape(all_chan_conditions, N_CB, N_CB, N_CB, N_CB);
shaped_snrs = reshape(all_snrs, N_CB, N_CB, N_CB, N_CB);

%% Convert SNR and channel condition to MIMO capacity
% See IEEE 802.11-15/0334r1

% MIMO capacity: \sum_i \log(1+P_i/N \sigma_i^2)
% We set P_i = P for non-waterfilling case (still-better capacity may be
% achievable with waterfilling, but I'll consider that beyond the scope of
% this script for now)
% Next, we need to determine the singular values of the channel matrix from
% the channel condition.  We want the squared singular values to sum to 2
sval1 = 2./(1+shaped_chan_c.^2);
mimo_cap = Fs * (log2(1+sval1.*shaped_snrs)+log2(1+(sval1.*shaped_chan_c.^2).*shaped_snrs));
% Ideal diversity = 9 dB SNR gain (4x from transmit without power constraint, 2x from rx)
diversity_cap = Fs*log2(1+8*shaped_snrs);



%% Basic comparisons

max_mmse = max(all_capacities_mmse, [], 'all');
max_zf = max(shaped_cap_zf, [], 'all');
max_mimo = max(mimo_cap, [], 'all');
max_mab = max(diversity_cap, [], 'all');
max_all = max(max_mmse, max(max_zf, max_mab));

figure(1);
% subplot(2,1,1);
% image(uint8((all_capacities_mmse/max_all)*255));
% title('MMSE');

subplot(1,2,1);
% image(uint8(reshape(mimo_cap, N_CB*N_CB, N_CB*N_CB)/max_all*255));
imagesc((reshape(mimo_cap, N_CB*N_CB, N_CB*N_CB)));
title('Multiplexing');

subplot(1,2,2);
% image(uint8(reshape(diversity_cap, N_CB*N_CB, N_CB*N_CB)/max_all*255));
imagesc((reshape(diversity_cap, N_CB*N_CB, N_CB*N_CB)));
title('MAB');

fprintf('Full MIMO: %e, MMSE: %e, ZF: %e, MAB: %e\n', max_mimo, max_mmse, max_zf, max_mab);

%% Evaluate k-best

K_MAX=10;

% Beamforming
cap_mab_k = zeros(1, K_MAX);
for k_val = 1:K_MAX
%     [sel_mab, cap_mab_k(k_val)] = beamform.sumimo_k_best(rx_ssw, tx_ssw, shaped_cap_mab, k_val);
    [sel_mab, cap_mab_k(k_val)] = beamform.sumimo_k_best(rx_ssw, tx_ssw, diversity_cap, k_val);
end

figure(2); clf; semilogx((1:K_MAX).^4, cap_mab_k); ylabel('Best capacity'); xlabel('k value');
hold on; plot((1:K_MAX).^4, ones(1,K_MAX)*max_mab,':'); hold off;
ylim([0 max_mab*1.2]);
title('K-best performance (beamforming)');

% Multiplexing
for k_val = 1:K_MAX
%     [sel_mult, cap_zf_k(k_val)] = beamform.sumimo_k_best(rx_ssw, tx_ssw, shaped_cap_zf, k_val);
    [sel_mult, cap_zf_k(k_val)] = beamform.sumimo_k_best(rx_ssw, tx_ssw, mimo_cap, k_val);
end

figure(3); semilogx((1:K_MAX).^4, cap_zf_k); ylabel('Best capacity'); xlabel('k value');
hold on; plot((1:K_MAX).^4, ones(1,K_MAX)*max_mimo,':'); hold off;
ylim([0 max_mimo*1.2]);
title('K-best performance (Multiplexing)');

%% Evaluate evolutionary approach

ITER_MAX = 4096;
SAMPLES = 32;
N_POINTS = 256;

% Beamforming
cap_mab_e = zeros(1, N_POINTS);
TRY_IDX = logspace(0,log10(ITER_MAX),N_POINTS);
% tic
% for iter_ii = 1:length(TRY_IDX)
%     iter_max = TRY_IDX(iter_ii);
%     for jj=1:SAMPLES
% %         [~, val] = beamform.sumimo_evolutionary(rx_ssw, tx_ssw, shaped_cap_mab, iter_max);
%         [~, val] = beamform.sumimo_evolutionary(rx_ssw, tx_ssw, diversity_cap, iter_max);
%         cap_mab_e(iter_ii) = cap_mab_e(iter_ii) + val;
%     end
%     cap_mab_e(iter_ii) = cap_mab_e(iter_ii)/SAMPLES;
% end
% toc
% 
% figure(2); hold on; semilogx(TRY_IDX,cap_mab_e); hold off



% Multiplexing
cap_zf_e = zeros(1, N_POINTS);
for iter_ii = 1:length(TRY_IDX)
    iter_max = TRY_IDX(iter_ii);
    for jj=1:SAMPLES
%         [~, val] = beamform.sumimo_evolutionary(rx_ssw, tx_ssw, shaped_cap_zf, iter_max);
        [~, val] = beamform.sumimo_evolutionary(rx_ssw, tx_ssw, mimo_cap, iter_max);
        cap_zf_e(iter_ii) = cap_zf_e(iter_ii) + val;
    end
    cap_zf_e(iter_ii) = cap_zf_e(iter_ii)/SAMPLES;
end

figure(3); hold on; semilogx(TRY_IDX, cap_zf_e);  hold off;

%% k-best v.2

% Beamforming
cap_mab_k2 = zeros(1, ITER_MAX);
TRY_IDX = 1:16:ITER_MAX;
for iter_max = TRY_IDX
    [~, cap_mab_k2(iter_max)] = beamform.sumimo_k_best_2(rx_ssw, tx_ssw, diversity_cap, iter_max);
end

figure(2); hold on; semilogx(TRY_IDX,cap_mab_k2(TRY_IDX)); hold off
legend({'K-best', 'Best Exhaustive', 'Evolutionary', 'K-best v.2'});

% Multiplexing
cap_zf_k2 = zeros(1, ITER_MAX);
for iter_max = TRY_IDX
    [~, cap_zf_k2(iter_max)] = beamform.sumimo_k_best_2(rx_ssw, tx_ssw, mimo_cap, iter_max);
end

figure(3); hold on; semilogx(TRY_IDX, cap_zf_k2(TRY_IDX));  hold off;
legend({'K-best', 'Best Exhaustive', 'Evolutionary', 'K-best v.2'});


%% Display

distFig('Rows', 2, 'Columns', 3);
