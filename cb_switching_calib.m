% load('rx_matrices/tx-sweep-tone.mat');

var_env = filter([zeros(1,30) ones(1,20)]/20,1,abs(diff(abs(rx_IQ))));

expected_sample_gap = 418 * 125e6 / 12e6;

start_idx = find(abs(rx_IQ)>0.02,1);
fprintf('Signal starts at sample %d\n', start_idx);
start_idx = 228;

expect_transition = zeros(1,length(rx_IQ)-1);
for ii=1:round(length(rx_IQ)/expected_sample_gap)
    idx = (-30:30) + round((ii-1)*expected_sample_gap) + start_idx;
    expect_transition(idx) = 1;
end

is_transition = (abs(diff(abs(rx_IQ)))>(6*var_env)) & expect_transition;

figure(1); subplot(2,1,1);
plot(abs(rx_IQ)); hold on; 
plot(abs(diff(abs(rx_IQ))));
plot(var_env*5);
plot(is_transition);
xlabel('Sample Number');
ylabel('Envelope');
hold off; axis tight;

transition_start = zeros(1,round(length(rx_IQ)/expected_sample_gap));
transition_end = zeros(size(transition_start));
transition_len = zeros(size(transition_start));
transition_time_err = zeros(size(transition_start));
transition_locs = zeros(81, length(transition_start));

for ii=1:length(transition_end)
    idx = (-30:50) + round((ii-1)*expected_sample_gap) + start_idx - 1;
    xition = idx(1) + find(is_transition(idx));
    transition_start(ii) = xition(1);
    transition_end(ii) = xition(end);
    transition_locs((transition_start(ii):transition_end(ii)) - idx(1),ii) = 1;
    transition_len(ii) = range(xition);
    expect_idx = round((ii-1)*expected_sample_gap) + start_idx - 1;
    transition_time_err(ii) = max(xition(1)-expect_idx, expect_idx-xition(end));
    if ii==1 || ii==2
        figure(2); subplot(2,1,ii);
        plot(abs(rx_IQ(idx)), 'LineWidth', 1.4);
        xlabel('Sample Number');
        ylabel('Envelope');
        if ii==1
            title('First command');
        else
            title('Later command (~20 sample transient)');
        end
    end
end
transition_time_err = transition_time_err .* (transition_time_err > 0);
% transition_time_err = (1 - 2*(mod(transition_start*expected_sample_gap)>100)) .* transition_time_err;

figure(1); subplot(2,1,2);
plot(transition_time_err, 'LineWidth', 2);
title('Transition Time Error');
xlabel('Transition Number');
ylabel('Timing Error (samples)');
axis tight;

figure(3);
imagesc(~transition_locs); colormap gray;
title('Estimated Transition Locations');