clearvars

N_PA = 2;
N_CB = 16;
CB_GAP = 300;          % Number of samples to allow for codebook switching
Fs = 62.5e6;             % 125 Msps sample rate
Fc = 3e9;               % 2.4 GHz center frequency
REPLAY_TIME = 1;       % Number of times for usrp to replay the wave
FPA = 12e6;

% Data-related params
N_OFDM_SYMS             = 32;          % Number of OFDM symbols (must be even valued)
MOD_ORDER               = 2;           % BPSK
INTERP_RATE             = 1;           % Interpolation rate (must be 2)

%% tx data generation
% Generate an OFDM signal for each codebook entry
rftx.lofdm_mimo2_gen_slow

txA_cbidx = repmat(1:N_CB, 1, N_CB*N_CB);
txB_cbidx = repmat(repelem(1:N_CB, 1, N_CB), 1, N_CB);

%% Assemble data, simulate a channel
xmit_unit_len = size(transmit_A,2)+2*CB_GAP;
pa_clk_count = round(xmit_unit_len/Fs*FPA);

% assert(pa_clk_count^2 <= 2^16-1);

xmit_idx = floor((0:N_CB^2-1)*pa_clk_count*Fs/FPA) + CB_GAP;
xmit_all_A = zeros(1,xmit_idx(end)+xmit_unit_len);
xmit_all_B = zeros(1,xmit_idx(end)+xmit_unit_len);
for ii=1:N_CB*N_CB
    txA_idx = mod(ii-1, N_CB) + 1;
    txB_idx = floor((ii-1)/N_CB) + 1;
    xmit_all_A((1:size(transmit_A,2))+xmit_idx(ii)) = transmit_A(txA_idx, :);
    xmit_all_B((1:size(transmit_A,2))+xmit_idx(ii)) = transmit_B(txB_idx, :);
end

rx_vec_air_A = xmit_all_A + 0.2 * exp(-2j) * xmit_all_B;
rx_vec_air_B = 0.9 * exp(-1.1j) * xmit_all_B + 0.1 * exp(-1j) * xmit_all_A;
rx_vec_air_A = circshift(rx_vec_air_A, 100);
rx_vec_air_B = circshift(rx_vec_air_B, 100);

rx_vec_air_A = rx_vec_air_A + 1e-2*complex(randn(1,length(rx_vec_air_A)), randn(1,length(rx_vec_air_A)));
rx_vec_air_B = rx_vec_air_B + 1e-2*complex(randn(1,length(rx_vec_air_B)), randn(1,length(rx_vec_air_B)));

rx_usrp_sw_B = [rx_vec_air_A; rx_vec_air_B];

%% Data processing
rx_data = rx_usrp_sw_B;
rfrx.lofdm_mimo2_proc3