function pa_ctl_tone_test(input_param)

if isequal(class(input_param), 'char')
    load(input_param, 'rx_IQ');
else
    rx_IQ = input_param;
end

Fs = 125e6;
FPA = 12e6;

figure(1);
plot(abs(rx_IQ));

envelope = abs(rx_IQ);
[assn, levels, ~] = kmeans(envelope(1:1000000)', 2);

stdev=[0 0]';
for ii=1:2
    stdev(ii)=std(envelope(assn==ii));
end

level_active = abs(envelope - levels) < 1.8*max(stdev);

level_active_smooth = level_active;
for smooth_shift = 1:3
    level_active_smooth = level_active_smooth(:,(smooth_shift+1):(end-smooth_shift)) & ...
        (level_active_smooth(:,(smooth_shift*2+1):end) | level_active_smooth(:,1:(end-2*smooth_shift)));
    level_active_smooth = [zeros(2,smooth_shift) level_active_smooth zeros(2,smooth_shift)];
end
smooth_shift = 1;
level_active_smooth = level_active_smooth(:,(smooth_shift+1):(end-smooth_shift)) | ...
    (level_active_smooth(:,(smooth_shift*2+1):end) & level_active_smooth(:,1:(end-2*smooth_shift)));
level_active_smooth = [zeros(2,smooth_shift) level_active_smooth zeros(2,smooth_shift)];

hold on; plot(level_active_smooth'); hold off;
xlim([0 10000]);

%% Find delay to first command
first_idx_1 = find(level_active_smooth(1,:), 1);
first_idx_2 = find(level_active_smooth(2,:), 1);

signal_starts = min(first_idx_1, first_idx_2);
first_cmd_complete = max(first_idx_1, first_idx_2);
cmd_delay = first_cmd_complete - signal_starts
% cmd_delay = first_cmd_complete - 203

%% Find transition periods
expected_sample_gap = 100 * Fs / FPA;
start_meas_idx = round(expected_sample_gap/2 + first_cmd_complete);
start_meas_idx = 203;
transitions = ~(level_active_smooth(1, start_meas_idx:end) | ...
    level_active_smooth(2, start_meas_idx:end));
num_transitions = floor(length(transitions)/expected_sample_gap);
xmit_idx = floor((0:num_transitions-1)*expected_sample_gap);

for ii=1:num_transitions
    transition_locs(:,ii) = transitions((1:round(expected_sample_gap))+xmit_idx(ii));
end

figure(3);
imagesc(~transition_locs); colormap gray;
title('Estimated Transition Locations');

%% Length of transition period
transition_length = sum(transition_locs,1);
transition_avg = mean(transition_length);
transition_std = std(transition_length);
transition_max = max(transition_length);

fprintf('Transition period length in samples: %.3f avg +/- %.3f, %d max\n', ...
    transition_avg, transition_std, transition_max);

%% Find clock drift
tx_idx = (1:1042)'.*transition_locs;
tx_idx(tx_idx==0) = NaN;
mean_cb_switch = nanmean(tx_idx, 1);
figure(5); plot(mean_cb_switch);
title('Transition location drift');
xlabel('Codebook switch iteration');
ylabel('Offset from expected switching time');

est_drift = polyfit(find(~isnan(mean_cb_switch)), mean_cb_switch(~isnan(mean_cb_switch)),1);
est_drift = est_drift(1)/expected_sample_gap;

fprintf('Estimated clock drift: %f ppm\n', est_drift);

%% Detect failed CB switching

cb1_activations = find(diff(level_active_smooth(1,:))==1);
cb2_activations = find(diff(level_active_smooth(2,:))==1);
% Note that both of these will have the same length, since the phased array
% config doesn't change, and this will cause a corresponding delay for both
% find(diff(cb1_activations)>3*expected_sample_gap)
% find(diff(cb2_activations)>3*expected_sample_gap)
pa_ctl_failure_rate = sum(diff(cb1_activations)>3*expected_sample_gap)/num_transitions;
fprintf('CB switching failure rate: %.3f%%\n', pa_ctl_failure_rate*100);