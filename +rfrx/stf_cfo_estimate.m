function cfo_est = stf_cfo_estimate(this_rx, stf_start, stf_end)

% Due to noise/CFO, not all STFs have necessarily been detected, so
% we'll estimate CFO using a region bounded by the 1st last predicted STF 
% locations, with a shift by 128
offsets = this_rx((stf_start+128):stf_end) .* conj(this_rx(stf_start:(stf_end-128)));
cfo_est = mean(unwrap(angle(offsets)))/128;
