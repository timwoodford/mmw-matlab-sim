function [stf_start, stf_end] = stf_coarse_sync(this_rx, stf_seg, DETECT_THRESH)

% If we can't get a good timesync initially, we'll try lowering the
% detection threshold until it looks like we're getting into the noise
% floor

assert(length(stf_seg)==128);

for thresh = linspace(DETECT_THRESH, 0.05, 15)
    xc_stf = xcorr(this_rx,stf_seg);
    xc_stf = xc_stf(length(this_rx):end);
    xc_stf_a = abs(xc_stf);
    stf_idx = find(xc_stf_a>thresh*max(xc_stf_a));
    ga_gaps = diff(stf_idx);
    bad_idx = stf_idx(find(ga_gaps<126 & ga_gaps>2)+1);
    if ~isempty(bad_idx)
        break
    end
    %     cef_idx = stf_idx(find(stf_idx(2:end)-stf_idx(1:end-1)==256)+1);
    % Gap of 127 may occur in high-CFO/noise where a peak gets blurred
    % accross multiple samples
    %     stf_best_mod = round(mod(stf_idx, 128)*(xc_stf_a(stf_idx))'/sum(xc_stf_a(stf_idx)));
    stf_best_mod = mode(mod(stf_idx, 128));
    stf_idx = stf_idx(mod(stf_idx, 128)==stf_best_mod);
%     figure(1); plot(abs(xc_stf_a));
%     line([1 length(xc_stf)], thresh*max(xc_stf_a)*[1 1], 'LineStyle', '--', 'Color', 'r', 'LineWidth', 2);
%     for ii=stf_idx
%         line([ii ii], [0 max(abs(xc_stf))], 'LineStyle', '--', 'Color', 'g', 'LineWidth', 1);
%     end
    stf_idx_last = stf_idx;
end

stf_idx = stf_idx_last;
stf_start = stf_idx(1);
stf_end = stf_idx(end)-1024;

if stf_end - stf_start ~= 2176
    warning('hi');
end