function [cef_start, cir] = cef_fine_sync(cef_region, Gb, Ga, Ga_align)

if Ga_align == 0
    Ga_align = 128;
end

xc_gb = xcorr(cef_region, Gb);
xc_gb = xc_gb(length(cef_region):end);
xc_gb_a = abs(xc_gb);

xc_ga = xcorr(cef_region, Ga);
xc_ga = xc_ga(length(cef_region):end);
xc_ga_a = abs(xc_ga);
xc_ga_128=xc_ga_a(Ga_align:128:end);
xc_gb_128=xc_gb_a(Ga_align:128:end);

[~, ga_idx] = maxk(xc_ga_128, 6);
[~, gb_idx] = maxk(xc_gb_128, 5);

aln_128 = mode(mod(gb_idx,2));
ga_idx = ga_idx(mod(ga_idx,2)==mod(aln_128+1,2));
gb_idx = gb_idx(mod(gb_idx,2)==aln_128);

% Handle edge case where later Gb may not have been among the maximums
if range(gb_idx) < 8
    gb_idx = [gb_idx 10-aln_128];
end

% We should now have the general region where the CEF is located
% assert(all(diff(sort(ga_idx))==2)); % Some of the gaps might be 1
if any(diff(sort(gb_idx))~=2)
    warning('CEF sync may be wrong');
end
cef_end_coarse = max(gb_idx)*128 + 128 + Ga_align;
cef_begin_coarse = max(cef_end_coarse - 10*128, 0);

% Allow +/- 24m range for multipath at 125 Msps, +/- 3m at 1 Gsps
% Should tune this probably for 1 GHz data collection
max_range_diff = 20;

% Gb xcorrs
xc_gb_f = zeros(5, 2*max_range_diff+1);
xc_gb_fa = zeros(5, 2*max_range_diff+1);
for ii=1:5
    jj=2*ii;
    xc_gb_f(ii,:) = xc_gb((-max_range_diff:max_range_diff) + jj*128 + cef_begin_coarse);
    xc_gb_fa(ii,:) = abs(xc_gb_f(ii,:));
end

% Ga xcorrs
xc_ga_f = zeros(4, 2*max_range_diff+1);
xc_ga_fa = zeros(4, 2*max_range_diff+1);
for ii=1:4
    xc_ga_f(ii,:) = xc_ga((-max_range_diff:max_range_diff) + (2*ii-1)*128 + cef_begin_coarse);
    xc_ga_fa(ii,:) = abs(xc_ga_f(ii,:));
end

% cir_a = sum(xc_gb_fa,1)+sum(xc_ga_fa,1);
% cir_a = cir_a((2*max_range_diff+1):end);
cir = sum(xc_gb_fa,1)+sum(xc_ga_fa,1);
% cir = cir((2*max_range_diff+1):end);
% figure; plot(abs(cir));

[~,cef_peak_idx] = max(cir);
cef_start = cef_begin_coarse + cef_peak_idx - 1 - max_range_diff;
cir_start_idx = cef_peak_idx-(length(cir)-cef_peak_idx);
cir_start_idx = (cir_start_idx > 0)*(cir_start_idx - 1) + 1;
cir = cir(cir_start_idx:end);

end