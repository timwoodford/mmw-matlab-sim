function [snr,cir] = cef_snr_est(rx_cef, Gb, Ga)

gb_coeff = [-1  0 +1  0 -1  0 -1  0]; % an extra -1 at the end
ga_coeff = [ 0 -1  0 -1  0 +1  0 -1]; % plus a zero at the end
% I leave out the suffix because we want equal numbers of Ga and Gb
% See Lei and Huang, CFR and SNR Estimation, 2009

seg_len = 128;
segments = (1:seg_len)+seg_len*(0:length(gb_coeff))';

xc_gb = zeros(length(gb_coeff), seg_len*2-1);
xc_ga = zeros(length(gb_coeff), seg_len*2-1);

for ii=1:length(gb_coeff)
    cef_seg = rx_cef(segments(ii,:));
%     figure(1); subplot(2,1,1); plot(abs(xcorr(cef_seg, Gb)));
%     hold on; plot(abs(gb_coeff(ii)*xcorr(cef_seg, Gb))); hold off;
%     subplot(2,1,2); plot(abs(xcorr(cef_seg, Ga)));
%     hold on; plot(abs(ga_coeff(ii)*xcorr(cef_seg, Ga))); hold off;
    xc_gb(ii,:) = gb_coeff(ii)*xcorr(cef_seg, Gb);
    xc_ga(ii,:) = ga_coeff(ii)*xcorr(cef_seg, Ga);
end

cir = (mean(xc_gb,1)+mean(xc_ga,1))/2;
% figure; plot(abs(cir));

% Get 4 CIRs obtained using Ga/Gb cancellation (Eq (12))
% cirs = (xc_gb+xc_ga);
cirs = (xc_gb.*exp(-1j*angle(xc_gb(:,127)))+xc_ga.*exp(-1j*angle(xc_ga(:,127))));
cirs = (cirs(1:2:end,:) + cirs(2:2:end,:))/2;

% Convert to channel frequency response (Eq (13))
cfrs = fftshift(fft(cirs, seg_len*2-1, 2),2);

% Estimate actual CFR - Eq (14)
cfr = mean(cfrs, 1);

% Estimate noise - Eq (15)
subcarr_noise = cfrs - cfr;

% Estimate power - Eq (16), (17)
sig_pwr = mean(abs(cfr).^2);
noise_pwr = mean(mean(abs(subcarr_noise).^2));

% SNR
snr = 10*log10(sig_pwr/noise_pwr);

end