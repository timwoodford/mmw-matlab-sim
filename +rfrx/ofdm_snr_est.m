function snr = ofdm_snr_est(rx_vals, tx_syms, H_est, sc_idx)
rx_syms = rx_vals ./ H_est;
evm_mat = abs(rx_syms(sc_idx,:) - tx_syms).^2;
aevms = mean(evm_mat(:));
snr = 10*log10(1./aevms);
end