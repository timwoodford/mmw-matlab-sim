% WARP-derived code Copyright (c) 2015 Mango Communications - All Rights Reserved
% Distributed under the WARP License (http://warpproject.org/license)

% Rx processing params
FFT_OFFSET                      = 4;            % Number of CP samples to use in FFT (on average)
LTS_CORR_THRESH                 = 0.7;          % Normalized threshold for LTS correlation
DO_APPLY_CFO_CORRECTION         = 1;            % Enable CFO estimation/correction
DO_APPLY_PHASE_ERR_CORRECTION   = 1;            % Enable Residual CFO estimation/correction
DO_APPLY_SFO_CORRECTION         = 1;            % Enable SFO estimation/correction

MOD_ORDER               = 2;            % Modulation order (2/4/16/64 = BSPK/QPSK/16-QAM/64-QAM)
TX_SCALE                = 1;          % Scale for Tx waveform ([0:1])
IQ_BALANCE              = 1;            % Calibrate the IQ imbalance

WRITE_PNG_FILES         = 0;

LTS_CORR_END            = 1400;

%% Split up beam sweep segments

rx_segments = zeros(length(xmit_idx), length(xmit_unit));
for ii=1:length(xmit_idx)
    rx_segments(ii,:) = rx_matrix(xmit_idx(ii)+(1:length(xmit_unit)));
end

%% OFDM SNR Estimation
lts_offs = zeros(1, length(xmit_idx));
snrs = zeros(1, length(xmit_idx));
rssi = zeros(1, length(xmit_idx));
for ii=1:length(xmit_idx)
    
    if DO_INTERPOLATION
        raw_rx_dec = filter(interp_filt2, 1, rx_segments(ii,:));
        raw_rx_dec = raw_rx_dec(1:2:end);
    else
        raw_rx_dec = rx_segments(ii,:);
    end
    % Complex cross correlation of Rx waveform with time-domain LTS
    lts_corr = abs(conv(conj(fliplr(ltf_t)), (raw_rx_dec)));
    
    % Skip early and late samples - avoids occasional false positives from pre-AGC samples
    lts_corr = lts_corr(32:end-32);
    
    % Artisinal, homegrown timesync system
    [m,best_pk]=max(sum(lts_corr((1:LTS_CORR_END)'+(0:length(ltf_t):4*length(ltf_t))),2));
    
    % Stop if no valid correlation peak was found
    % Check if correlations are likely to be bad
    if m < mean(lts_corr(1:LTS_CORR_END))*10 || max(lts_corr(1:LTS_CORR_END)) < 0.15
        if max(lts_corr(1:LTS_CORR_END)) > 0.1
            % Only warn if RSSI is low
%             figure; plot(lts_corr(1:LTS_CORR_END))
            warning('No LTS Correlation Peaks Found!');
        end
        snrs(ii) = NaN;
        lts_offs(ii) = NaN;
        best_pk = NaN;
        % Try to guess timesync and continue
        if ii > 4
            prev = lts_offs(1:(ii-1));
            if sum(~isnan(prev)) > 4
                best_pk = mode(prev(~isnan(prev))) - 128;
            end
        end
        if isnan(best_pk)
            continue
        end
    end
    
    % Set the sample indices of the payload symbols and preamble
    % The "+32" corresponds to the 32-sample cyclic prefix on the preamble LTS
    % The "-160" corresponds to the length of the preamble LTS (2.5 copies of 64-sample LTS)
    payload_ind = best_pk + 4*length(ltf_t) + 32;
    lts_ind = payload_ind-160;
    if sum(~isnan(lts_offs(1:ii))) > 7 && abs(lts_ind-mode(lts_offs(1:ii))) > 50
        % Fix obviously wrong syncs
        lts_ind = mode(lts_offs(1:ii));
        payload_ind = lts_ind + 160;
    else
        lts_offs(ii) = lts_ind;
    end
    
%     fprintf('LTS starts at %d\n', lts_ind);
    
    %Extract LTS (not yet CFO corrected)
    rx_lts = raw_rx_dec(lts_ind : lts_ind+159);
    rx_lts1 = rx_lts(-64+-FFT_OFFSET + [97:160]);
    rx_lts2 = rx_lts(-FFT_OFFSET + [97:160]);
    
    %Calculate coarse CFO est
    rx_cfo_est_lts = mean(unwrap(angle(rx_lts2 .* conj(rx_lts1))));
    rx_cfo_est_lts = rx_cfo_est_lts/(2*pi*64);
    
    % Apply CFO correction to raw Rx waveform
    rx_cfo_corr_t = exp(-1i*2*pi*rx_cfo_est_lts*[0:length(raw_rx_dec)-1]);
    rx_dec_cfo_corr = raw_rx_dec .* rx_cfo_corr_t;
    
    % Re-extract LTS for channel estimate
    rx_lts = rx_dec_cfo_corr(lts_ind-128 : lts_ind+159);
    rx_lts1 = rx_lts(-64+-FFT_OFFSET + [97:160]);
    rx_lts2 = rx_lts(-FFT_OFFSET + [97:160]);
    rx_lts3 = rx_lts(64-FFT_OFFSET + [97:160]);
    rx_lts4 = rx_lts(128-FFT_OFFSET + [97:160]);
    
    rx_lts1_f = fft(rx_lts1);
    rx_lts2_f = fft(rx_lts2);
    rx_lts3_f = fft(rx_lts2);
    rx_lts4_f = fft(rx_lts2);
    
    % Calculate channel estimate from average of 2 training symbols
    rx_H_est = ltf_f .* (rx_lts1_f + rx_lts4_f)/2;
    rssi(ii) = mean(abs(rx_H_est));
%     rx_H_est = ltf_f .* (rx_lts1_f + rx_lts2_f + rx_lts3_f + rx_lts4_f)/4;
    
    % Extract the payload samples (integral number of OFDM symbols following preamble)
    payload_vec = rx_dec_cfo_corr(payload_ind : payload_ind+N_OFDM_SYMS*(N_SC+CP_LEN)-1);
    payload_mat = reshape(payload_vec, (N_SC+CP_LEN), N_OFDM_SYMS);
    
    % Remove the cyclic prefix, keeping FFT_OFrx_segments(ii,:)FSET samples of CP (on average)
    payload_mat_noCP = payload_mat(CP_LEN-FFT_OFFSET+[1:N_SC], :);
    
    % Take the FFT
    syms_f_mat = fft(payload_mat_noCP, N_SC, 1);
    
    % Equalize (zero-forcing, just divide by complex chan estimates)
    syms_eq_mat = syms_f_mat ./ repmat(rx_H_est.', 1, N_OFDM_SYMS);
    
    % SFO
    % SFO manifests as a frequency-dependent phase whose slope increases
    % over time as the Tx and Rx sample streams drift apart from one
    % another. To correct for this effect, we calculate this phase slope at
    % each OFDM symbol using the pilot tones and use this slope to
    % interpolate a phase correction for each data-bearing subcarrier.
    
    % Extract the pilot tones and "equalize" them by their nominal Tx values
    pilots_f_mat = syms_eq_mat(SC_IND_PILOTS, :);
    pilots_f_mat_comp = pilots_f_mat.*pilots_mat;
    
    % Calculate the phases of every Rx pilot tone
    pilot_phases = unwrap(angle(fftshift(pilots_f_mat_comp,1)), [], 1);
    
    % Calculate slope of pilot tone phases vs frequency in each OFDM symbol
    pilot_spacing_mat = repmat(mod(diff(fftshift(SC_IND_PILOTS)),64).', 1, N_OFDM_SYMS);
    pilot_slope_mat = mean(diff(pilot_phases) ./ pilot_spacing_mat);
    
    % Calculate the SFO correction phases for each OFDM symbol
    pilot_phase_sfo_corr = fftshift((-32:31).' * pilot_slope_mat, 1);
    pilot_phase_corr = exp(-1i*(pilot_phase_sfo_corr));
    
    % Apply the pilot phase correction per symbol
    syms_eq_mat = syms_eq_mat .* pilot_phase_corr;
    
    
    % Correct for phase jitter
    % Extract the pilots and calculate per-symbol phase error
    pilots_f_mat = syms_eq_mat(SC_IND_PILOTS, :);
    pilots_f_mat_comp = pilots_f_mat.*pilots_mat;
    pilot_phase_err = angle(mean(pilots_f_mat_comp));
    pilot_phase_err_corr = repmat(pilot_phase_err, N_SC, 1);
    pilot_phase_corr = exp(-1i*(pilot_phase_err_corr));
    
    % Apply the pilot phase correction per symbol
    syms_eq_pc_mat = syms_eq_mat .* pilot_phase_corr;
    payload_syms_mat = syms_eq_pc_mat(SC_IND_DATA, :);
    
    % SNR
    evm_mat = abs(payload_syms_mat - tx_syms_mat).^2;
    aevms = mean(evm_mat(:));
    snrs(ii) = 10*log10(1./aevms);
end

%% Plots & such

figure(2); hold on; scatter(mag2db(rssi), snrs, '+', 'LineWidth', 2);
title('SNR vs. RSS');
xlabel('Relative RSSI (dB)');
ylabel('SNR (dB)');

figure(3); 
subplot(2,1,1); bar(snrs);
title('Per-beam SNR');
xlabel('Beam index');
ylabel('SNR');

subplot(2,1,2); bar(rssi);
title('Per-beam RSS');
xlabel('Beam index');
ylabel('RSSI');

return
%% Plots for paper
% Made using xcheck_190722-175533.mat

figure(4); clf; stairs(rssi, 'LineWidth', 2);
title('Per-beam RSS');
xlabel('Beam Index');
ylabel('Relative RSS (dB)');
axis tight;

axes('Position', [0.18 0.62 0.3 0.27]);
box on;
subp_idx = 11:15;
labels=subp_idx*(length(tx_ofdm_payload)+2*cb_gap)/Fs*1e3;
stairs(labels, rssi(subp_idx), 'LineWidth', 2, 'Color', 'r');
set(gca, 'Yticklabel', [])
% set(gca, 'Xticklabel', [])
% ylabel('Relative RSS (dB)');
xlabel('Time (ms)');
grid on;
xlim([-inf inf]);
ymax = -inf;
for ii=1:(length(subp_idx)-1)
    xval = mean(labels(ii:ii+1));
    yval = rssi(subp_idx(ii));
    t=text(xval, yval, sprintf('Beam\n%2d', subp_idx(ii)));
    t.Position(2) = t.Position(2) + t.Extent(4)*0.7;
    t.HorizontalAlignment = 'center';
    ymax = max(ymax, t.Extent(2)+t.Extent(4));
end
ylim([-inf ymax*1.03]);

%% Another plot
rx_H_est_plot = repmat(complex(NaN,NaN),1,length(rx_H_est));
rx_H_est_plot(SC_IND_DATA) = rx_H_est(SC_IND_DATA);
rx_H_est_plot(SC_IND_PILOTS) = rx_H_est(SC_IND_PILOTS);

x = (20/N_SC) * (-(N_SC/2):(N_SC/2 - 1));

figure;
stairs(x, fftshift(abs(rx_H_est_plot)),'LineWidth', 2);
title('Channel Estimate')
xlim([-inf inf])
ylabel('Subcarrier index')
xlabel('Subcarrier index')
ylabel('Channel Estimate Magnitude')
ylim([0 1.4])
grid on
title('Channel Estimate (Low SNR)')
