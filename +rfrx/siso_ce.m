%% Parameters
clearvars; close all
DETECT_THRESH=0.8;
load('ofdm_data/collect_ssw_xcheck_190711-160110.mat');

%% Initial per-packet processing
cfos=zeros(1,length(xmit_idx));
rss=zeros(1,length(xmit_idx));
snrs=zeros(1,length(xmit_idx));
cef_start_vals=zeros(1,length(xmit_idx));
% Note that we can't get phases at this stage because we have to
% re-estimate the CFO for each codebook entry.  If you need relative phase,
% we can probably use the results from here to estimate CFO, then do
% global CFO correction
for cb_idx=0:(length(xmit_idx)-1)
    this_rx = rx_matrix((1:(length(xmit_unit)))+xmit_idx(cb_idx+1));
    
    % Step 1: STF coarse timesync
   
    [stf_start, stf_end] = rfrx.stf_coarse_sync(this_rx, stf(1:128), DETECT_THRESH);
    
    if stf_end - stf_start < 1024
        fprintf("Couln't find STFs for CFO estimation\n");
        continue
    end
    
    % Step 2: estimate CFO
    % Reduce stf_end by 128 because the last Ga may be part of the CEF
    % This will cause a phase offset because the STF and CEF are pi/2
    % rotated separately
    cfo_est = rfrx.stf_cfo_estimate(this_rx, stf_start, stf_end-128);
    cfos(cb_idx+1) = cfo_est;
    this_rx_cfo = this_rx.*exp(-1j*cfo_est*(1:length(this_rx)));
    
    % Step 3: Re-estimate timing offset
    [cef_start, cir] = rfrx.cef_fine_sync(this_rx(stf_len+cb_gap:end), ...
                           stf(1:128), cef(129:256), mod(stf_end, 128));
    cef_start = cef_start + stf_len+cb_gap;
    
    
    % First pass: if we haven't successfully found the CEF, set SNR=0
    if cef_start + length(cef) > length(this_rx) || cef_start < length(stf)
        rss(cb_idx+1)=0;
        snrs(cb_idx+1)=0;
        cef_start_vals(cb_idx+1) = NaN;
        cfos(cb_idx+1)=NaN;
        continue
    end
    
    cef_start_vals(cb_idx+1) = cef_start;
    
    % Step 4: SC channel estimation
    rx_cef = this_rx_cfo((1:length(cef))+cef_start-1);
    sc_chanest = (rx_cef*cef')/(cef*cef');
    
    % Step 5: Comparison and SNR estimation
    [ofdm_est, snr_ofdm] = rfrx.cef_est_ofdm(rx_cef, cef);
    
    rss(cb_idx+1)=abs(sc_chanest);
    snrs(cb_idx+1)=snr_ofdm;
end

%% Find relative phases
% This requires doing global CFO correction
% Find CFO from highest-SNR
rss_nan = rss((~isnan(rss))&(~isnan(cfos)));
cfo_nan = cfos((~isnan(rss))&(~isnan(cfos)));
cfo_est = rss_nan*cfo_nan'/sum(rss_nan);
rx_cfo_corr = rx_matrix.*exp((1:length(rx_matrix))*cfo_est*-1j);
sc_ces = zeros(1,length(xmit_idx));
for cb_idx=0:(length(xmit_idx)-1)
    this_rx = rx_cfo_corr((1:(length(xmit_unit)))+xmit_idx(cb_idx+1));
    % Step 1: STF coarse timesync
    [stf_start, stf_end] = rfrx.stf_coarse_sync(this_rx, stf(1:128), DETECT_THRESH);
    assert(stf_end - stf_start <= length(stf));
    
    % Step 2: estimate CFO
    cfo_est = rfrx.stf_cfo_estimate(this_rx, stf_start, stf_end-128);
    cfos2(cb_idx+1) = cfo_est;
    this_rx_cfo = this_rx.*exp(-1j*cfo_est*(1:length(this_rx)));
    
    % Step 3: Find beginning of CEF
    [cef_start, cir] = rfrx.cef_fine_sync(this_rx(stf_len+cb_gap:end), ...
                           stf(1:128), cef(129:256), mod(stf_end, 128));
    cef_start = cef_start + stf_len+cb_gap;
    if abs(cef_start-median(cef_start_vals)) > 90
        cef_start = cef_start+round((mean(cef_start_vals)-cef_start)/128)*128;
    end
    cef_starts_2(cb_idx+1)=cef_start;
    rx_cef = this_rx_cfo((1:length(cef))+cef_start-1);

    if cef_start + length(cef) > length(this_rx) || cef_start < length(stf)
        error('Indexes are bad');
    end

    rss2(cb_idx+1) = abs(sc_chanest);
    rss3(cb_idx+1) = mean(abs(rx_cef).^2);
    sc_ces(cb_idx+1) = sc_chanest;
    
    [ofdm_est, snr] = rfrx.cef_est_ofdm(rx_cef, cef);
    snrs3(cb_idx+1) = snr;
    [snr, cir] = rfrx.cef_snr_est(rx_cef, stf(1:128), cef(129:256));
    snrs4(cb_idx+1) = snr;
    figure(3); plot(abs(cir)); title('CIR');  
    figure(5); hold on; plot(abs(ofdm_est));
end

%% Plot some things

figure(4);
subplot(2,1,1);
bar(snrs4);
title('Per-beam SNR');
subplot(2,1,2);
bar(rss3);
title('Per-beam RSS');

figure(8);
plot(rss3, snrs4,'.');
rsfit = polyfit(rss3, snrs4, 1);
rsx = linspace(min(rss3), max(rss3), 10);
rsline = rsfit(1)*rsx + rsfit(2);
hold on; plot(rsx, rsline); hold off;
title('RSS vs SNR');
xlabel('RSS');
ylabel('SNR');

distFig('Rows',2,'Columns',3)

