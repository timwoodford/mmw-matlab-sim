% Do basic OFDM processing from a set of exhaustive beam training runs

% WARP-derived code Copyright (c) 2015 Mango Communications - All Rights Reserved
% Distributed under the WARP License (http://warpproject.org/license)

LTS_CORR_THRESH               = 0.8; % Lower for low-SNR situations
FFT_OFFSET                    = 4;   % Number of CP samples to use in FFT (on average)
% data = matfile('data/mimo/los/collect_mimo2_exhaustive_190722-160805.mat');
% load(data.Properties.Source, '-regexp', '^(?!rx_\w)\w');

%% Decimate
if(INTERP_RATE == 1)
    raw_rx_dec_A = rx_data(1,:);
    raw_rx_dec_B = rx_data(2,:);
elseif(INTERP_RATE == 2)
    raw_rx_dec_A = filter(interp_filt2, 1, rx_data(1,:));
    raw_rx_dec_A = raw_rx_dec_A(1:2:end);
    raw_rx_dec_B = filter(interp_filt2, 1, rx_data(2,:));
    raw_rx_dec_B = raw_rx_dec_B(1:2:end);
else
    error('Unknown decimation rate');
end

%% Split up segments
num_seg = length(xmit_idx);

% rx_segments_A = reshape(raw_rx_dec_A(1:mod_len), unit_len, num_seg);
% rx_segments_B = reshape(raw_rx_dec_B(1:mod_len), unit_len, num_seg);

%% Timesync & CFO Estimation
mimo_lts_A = zeros(192, num_seg);
mimo_lts_B = zeros(192, num_seg);
payload_A = zeros(N_SC, N_OFDM_SYMS, num_seg);
payload_B = zeros(N_SC, N_OFDM_SYMS, num_seg);
start_idx = zeros(1, num_seg);
est_noise = zeros(1, num_seg);
rx_H_est = zeros(N_SC, 2, 2, num_seg);

for seg_ii=1:num_seg
    this_rx_A = raw_rx_dec_A(xmit_idx(seg_ii) + (1:xmit_unit_len));
    this_rx_B = raw_rx_dec_B(xmit_idx(seg_ii) + (1:xmit_unit_len));
    
    % For simplicity, we'll only use RFA for LTS correlation and peak
    % discovery. A straightforward addition would be to repeat this process for
    % RFB and combine the results for detection diversity.
    
    % Complex cross correlation of Rx waveform with time-domain LTS
    lts_corr = abs(conv(conj(fliplr(lts_t)), this_rx_A));
    if mod(seg_ii,100)==1
        %     figure(1); plot(lts_corr)
    end
    
    % Skip early and late samples - avoids occasional false positives from pre-AGC samples
    lts_corr = lts_corr(32:end-32);
    
    % Find all correlation peaks
    lts_peaks = find(lts_corr > LTS_CORR_THRESH*max(lts_corr));
    
    % Select best candidate correlation peak as LTS-payload boundary
    % In this MIMO example, we actually have 3 LTS symbols sent in a row.
    % The first two are sent by RFA on the TX node and the last one was sent
    % by RFB. We will actually look for the separation between the first and the
    % last for synchronizing our starting index.
    
    [LTS1, LTS2] = meshgrid(lts_peaks,lts_peaks);
    [lts_last_peak_index,y] = find(LTS2-LTS1 == length(lts_t));
    
    % Stop if no valid correlation peak was found
    if(isempty(lts_last_peak_index))
        start_idx(seg_ii) = NaN;
        continue
    end
    
    % Set the sample indices of the payload symbols and preamble
    % The "+32" here corresponds to the 32-sample cyclic prefix on the preamble LTS
    % The "+192" corresponds to the length of the extra training symbols for MIMO channel estimation
    mimo_training_ind = lts_peaks(max(lts_last_peak_index)) + 32;
    payload_ind = mimo_training_ind + 192;
    
    % Subtract of 2 full LTS sequences and one cyclic prefixes
    % The "-160" corresponds to the length of the preamble LTS (2.5 copies of 64-sample LTS)
    lts_ind = mimo_training_ind-160;
    
    % Sanity-check sync
    if payload_ind+N_OFDM_SYMS*(N_SC+CP_LEN)-1 > length(this_rx_A)
        start_idx(seg_ii) = NaN;
        continue;
    end
    
    start_idx(seg_ii) = lts_ind;
    
    %Extract LTS (not yet CFO corrected)
    rx_lts = this_rx_A(lts_ind : lts_ind+159); %Extract the first two LTS for CFO
    rx_lts1 = rx_lts(-64+-FFT_OFFSET + [97:160]);
    rx_lts2 = rx_lts(-FFT_OFFSET + [97:160]);
    
    %Calculate coarse CFO est
    rx_cfo_est_lts = mean(unwrap(angle(rx_lts2 .* conj(rx_lts1))));
    rx_cfo_est_lts = rx_cfo_est_lts/(2*pi*64);
    
    % Apply CFO correction to raw Rx waveforms
    rx_cfo_corr_t = exp(-1i*2*pi*rx_cfo_est_lts*[0:length(this_rx_A)-1]);
    rx_dec_cfo_corr_A = this_rx_A .* rx_cfo_corr_t;
    rx_dec_cfo_corr_B = this_rx_B .* rx_cfo_corr_t;
    
    % Estimate noise
    rx_lts = rx_dec_cfo_corr_A(lts_ind : lts_ind+159);
    rx_lts1 = rx_lts(-64+-FFT_OFFSET + [97:160]);
    rx_lts2 = rx_lts(-FFT_OFFSET + [97:160]);
    est_noise(seg_ii) = mean(abs(rx_lts2-rx_lts2).^2);
    
    % MIMO Estimation
    
    % MIMO Channel Estimatation
    lts_ind_TXA_start = mimo_training_ind + 32 - FFT_OFFSET;
    lts_ind_TXA_end = lts_ind_TXA_start + 64 - 1;
    
    lts_ind_TXB_start = mimo_training_ind + 32 + 64 + 32 - FFT_OFFSET;
    lts_ind_TXB_end = lts_ind_TXB_start + 64 - 1;
    
    rx_lts_BA = rx_dec_cfo_corr_A( lts_ind_TXA_start:lts_ind_TXA_end );
    rx_lts_AA = rx_dec_cfo_corr_A( lts_ind_TXB_start:lts_ind_TXB_end );
    
    rx_lts_BB = rx_dec_cfo_corr_B( lts_ind_TXA_start:lts_ind_TXA_end );
    rx_lts_AB = rx_dec_cfo_corr_B( lts_ind_TXB_start:lts_ind_TXB_end );
    
    rx_lts_AA_f = fft(rx_lts_AA);
    rx_lts_BA_f = fft(rx_lts_BA);
    
    rx_lts_AB_f = fft(rx_lts_AB);
    rx_lts_BB_f = fft(rx_lts_BB);
    
    % Calculate channel estimate
    rx_H_est_AA = lts_f .* rx_lts_AA_f;
    rx_H_est_BA = lts_f .* rx_lts_BA_f;
    
    rx_H_est_AB = lts_f .* rx_lts_AB_f;
    rx_H_est_BB = lts_f .* rx_lts_BB_f;
    
    % Store in matrix
    rx_H_est(:,1,1,seg_ii) = rx_H_est_AA;
    rx_H_est(:,1,2,seg_ii) = rx_H_est_BA;
    rx_H_est(:,2,1,seg_ii) = rx_H_est_AB;
    rx_H_est(:,2,2,seg_ii) = rx_H_est_BB;
    
    % Split up the remaining signal
    
    payload_vec_A = rx_dec_cfo_corr_A(payload_ind : payload_ind+N_OFDM_SYMS*(N_SC+CP_LEN)-1);
    payload_mat_A = reshape(payload_vec_A, (N_SC+CP_LEN), N_OFDM_SYMS);
    payload_vec_B = rx_dec_cfo_corr_B(payload_ind : payload_ind+N_OFDM_SYMS*(N_SC+CP_LEN)-1);
    payload_mat_B = reshape(payload_vec_B, (N_SC+CP_LEN), N_OFDM_SYMS);
    
    % Remove the cyclic prefix, keeping FFT_OFFSET samples of CP (on average)
    payload_A(:,:,seg_ii) = fft(payload_mat_A(CP_LEN-FFT_OFFSET+(1:N_SC), :), N_SC, 1);
    payload_B(:,:,seg_ii) = fft(payload_mat_B(CP_LEN-FFT_OFFSET+(1:N_SC), :), N_SC, 1);
    
end

%% Check if we have any syncs at all
% Prealloc results in case we need to return early
capacity_multiplexing_mmse = zeros(1, num_seg);
capacity_multiplexing_zf = zeros(1, num_seg);
capacity_beamf = zeros(1, num_seg);
channel_condition = zeros(1, num_seg);

if all(isnan(start_idx))
    return
end

%% Pilot-based correction
pilot_phase_errors = zeros(N_OFDM_SYMS, num_seg);
payload_A_corr = zeros(N_SC, N_OFDM_SYMS, num_seg);
payload_B_corr = zeros(N_SC, N_OFDM_SYMS, num_seg);

for seg_ii=1:num_seg
    % Equalize pilots
    % Because we only used Tx RFA to send pilots, we can do SISO equalization
    % here. This is zero-forcing (just divide by chan estimates)
    syms_eq_mat_pilots = payload_A(:,:,seg_ii) ./ repmat(rx_H_est(:,1,1,seg_ii), 1, N_OFDM_SYMS);
    
    % SFO manifests as a frequency-dependent phase whose slope increases
    % over time as the Tx and Rx sample streams drift apart from one
    % another. To correct for this effect, we calculate this phase slope at
    % each OFDM symbol using the pilot tones and use this slope to
    % interpolate a phase correction for each data-bearing subcarrier.
    
    % Extract the pilot tones and "equalize" them by their nominal Tx values
    pilots_f_mat = syms_eq_mat_pilots(SC_IND_PILOTS,:);
    pilots_f_mat_comp = pilots_f_mat.*pilots_mat_A;
    
    % Calculate the phases of every Rx pilot tone
    pilot_phases = unwrap(angle(fftshift(pilots_f_mat_comp, 1)), [], 1);
    
    pilot_spacing_mat = repmat(mod(diff(fftshift(SC_IND_PILOTS)), 64).', 1, N_OFDM_SYMS);
    pilot_slope_mat = mean(diff(pilot_phases) ./ pilot_spacing_mat);
    
    % Calculate the SFO correction phases for each OFDM symbol
    pilot_phase_sfo_corr = fftshift((-32:31).' * pilot_slope_mat, 1);
    pilot_phase_corr = exp(-1i*(pilot_phase_sfo_corr));
    
    % Apply the pilot phase correction per symbol
    syms_f_mat_A = payload_A(:,:,seg_ii) .* pilot_phase_corr;
    syms_f_mat_B = payload_B(:,:,seg_ii) .* pilot_phase_corr;
    
    % Extract the pilots and calculate per-symbol phase error
    pilots_f_mat = syms_eq_mat_pilots(SC_IND_PILOTS, :);
    pilot_phase_err = angle(mean(pilots_f_mat.*pilots_mat_A));
    pilot_phase_corr = repmat(exp(-1i*pilot_phase_err), N_SC, 1);
    
    % Save the symbol-wide phase error for later analysis.
    % This is potentially interesting to us because I suspect that there is
    % substantial random phase offset over time between different clocks,
    % beyond just residual CFO
    pilot_phase_errors(:,seg_ii) = pilot_phase_err;
    
    % Apply pilot phase correction to both received streams
    payload_A_corr(:,:,seg_ii) = syms_f_mat_A .* pilot_phase_corr;
    payload_B_corr(:,:,seg_ii) = syms_f_mat_B .* pilot_phase_corr;
end

%% MIMO
% This is where things get interesting for us
% We have 3 main options here, each with different capacity characteristics
% * Spatial multiplexing (ZF, MMSE)
% * Open-loop MIMO - ratio combining at rx
% * Closed-loop MIMO - ratio combining at tx and rx

% Spatial multiplexing (ZF, MMSE)
capacity_multiplexing_zf = zeros(1, num_seg);
channel_condition = zeros(1, num_seg);
ALL_SC = [SC_IND_DATA, SC_IND_PILOTS];
for seg_ii=1:num_seg
    % Retrieve H
    rx_H_est_AA = rx_H_est(:,1,1,seg_ii);
    rx_H_est_BA = rx_H_est(:,1,2,seg_ii);
    rx_H_est_AB = rx_H_est(:,2,1,seg_ii);
    rx_H_est_BB = rx_H_est(:,2,2,seg_ii);
    syms_f_mat_pc_A = payload_A_corr(:,:,seg_ii);
    syms_f_mat_pc_B = payload_B_corr(:,:,seg_ii);
    syms_eq_mat_A = zeros(N_SC, N_OFDM_SYMS);
    syms_eq_mat_B = zeros(N_SC, N_OFDM_SYMS);
    channel_condition_mat = zeros(1,N_SC);
    for sc_idx = ALL_SC
        y = [syms_f_mat_pc_A(sc_idx,:) ; syms_f_mat_pc_B(sc_idx,:)];
        H = [rx_H_est_AA(sc_idx), rx_H_est_BA(sc_idx) ; rx_H_est_AB(sc_idx), rx_H_est_BB(sc_idx)];
        % Zero-forcing goes here
        x = H\y;
        syms_eq_mat_A(sc_idx, :) = x(1,:);
        syms_eq_mat_B(sc_idx, :) = x(2,:);
        channel_condition_mat(sc_idx) = rcond(H);
    end
    channel_condition(seg_ii) = mean(channel_condition_mat);
    % Grab the correct original payload
    tx_syms_mat_A = reshape(tx_syms_A(txA_cbidx(seg_ii),:), length(SC_IND_DATA), N_OFDM_SYMS);
    tx_syms_mat_B = reshape(tx_syms_B(txB_cbidx(seg_ii),:), length(SC_IND_DATA), N_OFDM_SYMS);
    % Chop out the data channels.  We're doing OFDMA on top of spatial
    % multiplexing here, so we'll manually cause interference for this case
    rx_syms_mat_A = syms_eq_mat_A(SC_IND_DATA, :);
    rx_syms_mat_B = syms_eq_mat_B(SC_IND_DATA, :);
    % Do EVM-based capacity estimation
    capacity_multiplexing_zf(seg_ii) = ...
        capacity2(rx_syms_mat_A, rx_syms_mat_B, tx_syms_mat_A, tx_syms_mat_B, Fs);
end


% Spatial multiplexing (MMSE)
capacity_multiplexing_mmse = zeros(1, num_seg);
for seg_ii=1:num_seg
    rx_H_est_AA = rx_H_est(:,1,1,seg_ii);
    rx_H_est_BA = rx_H_est(:,1,2,seg_ii);
    rx_H_est_AB = rx_H_est(:,2,1,seg_ii);
    rx_H_est_BB = rx_H_est(:,2,2,seg_ii);
    syms_f_mat_pc_A = payload_A_corr(:,:,seg_ii);
    syms_f_mat_pc_B = payload_B_corr(:,:,seg_ii);
    syms_eq_mat_A = zeros(N_SC, N_OFDM_SYMS);
    syms_eq_mat_B = zeros(N_SC, N_OFDM_SYMS);
    for sc_idx = ALL_SC
        y = [syms_f_mat_pc_A(sc_idx,:) ; syms_f_mat_pc_B(sc_idx,:)];
        H = [rx_H_est_AA(sc_idx), rx_H_est_BA(sc_idx) ; rx_H_est_AB(sc_idx), rx_H_est_BB(sc_idx)];
        if all(H==0)
            continue
        end
        noise = est_noise(seg_ii);
        % MMSE goes here - only difference from ZF is use of noise terms
        x = (H'*H+noise*eye(2))\(H'*y);
        syms_eq_mat_A(sc_idx, :) = x(1,:);
        syms_eq_mat_B(sc_idx, :) = x(2,:);
    end
    % Grab the correct original payload
    tx_syms_mat_A = reshape(tx_syms_A(txA_cbidx(seg_ii),:), length(SC_IND_DATA), N_OFDM_SYMS);
    tx_syms_mat_B = reshape(tx_syms_B(txB_cbidx(seg_ii),:), length(SC_IND_DATA), N_OFDM_SYMS);
    % Chop out the data channels.
    rx_syms_mat_A = syms_eq_mat_A(SC_IND_DATA, :);
    rx_syms_mat_B = syms_eq_mat_B(SC_IND_DATA, :);
    % Do EVM-based capacity estimation
    capacity_multiplexing_mmse(seg_ii) = ...
        capacity2(rx_syms_mat_A, rx_syms_mat_B, tx_syms_mat_A, tx_syms_mat_B, Fs);
end

% Open-loop MIMO - maybe not be possible with current signal

% Closed-loop MIMO (MRC)

% Channel condition
channel_condition = zeros(1, num_seg);
channel_mat_vals = zeros(2,2,length(SC_IND_DATA),num_seg);
for seg_ii=1:num_seg
    rx_H_est_AA = rx_H_est(:,1,1,seg_ii);
    rx_H_est_BA = rx_H_est(:,1,2,seg_ii);
    rx_H_est_AB = rx_H_est(:,2,1,seg_ii);
    rx_H_est_BB = rx_H_est(:,2,2,seg_ii);
    channel_condition_mat = zeros(1,N_SC);
    for sc_idx = [SC_IND_DATA, SC_IND_PILOTS]
        H = [rx_H_est_AA(sc_idx), rx_H_est_BA(sc_idx) ; rx_H_est_AB(sc_idx), rx_H_est_BB(sc_idx)];
        channel_condition_mat(sc_idx) = 1/cond(H);
        channel_mat_vals(:,:,sc_idx,seg_ii) = H;
    end
    channel_condition(seg_ii) = mean(channel_condition_mat([SC_IND_DATA, SC_IND_PILOTS]));
end

%% MAB estimation

capacity_beamf = zeros(1, num_seg);
snrs = zeros(1, length(SC_IND_DATA));
snr_est_out = zeros(1, num_seg);
for seg_ii=1:num_seg
    % Retrieve H
    rx_H_est_AA = rx_H_est(:,1,1,seg_ii);
    rx_H_est_BA = rx_H_est(:,1,2,seg_ii);
    rx_H_est_AB = rx_H_est(:,2,1,seg_ii);
    rx_H_est_BB = rx_H_est(:,2,2,seg_ii);
    syms_f_mat_pc_A = payload_A_corr(:,:,seg_ii);
    syms_f_mat_pc_B = payload_B_corr(:,:,seg_ii);
    channel_condition_mat = zeros(1,N_SC);
    % Grab the correct original payload
    tx_syms_mat_A = reshape(tx_syms_A(txA_cbidx(seg_ii),:), length(SC_IND_DATA), N_OFDM_SYMS);
    tx_syms_mat_B = reshape(tx_syms_B(txB_cbidx(seg_ii),:), length(SC_IND_DATA), N_OFDM_SYMS);
    for sc_ii = 1:length(SC_IND_DATA)
        sc_idx = SC_IND_DATA(sc_ii);
        y = [syms_f_mat_pc_A(sc_idx,:) ; syms_f_mat_pc_B(sc_idx,:)];
        H = [rx_H_est_AA(sc_idx), rx_H_est_BA(sc_idx) ; rx_H_est_AB(sc_idx), rx_H_est_BB(sc_idx)];
        if all(H==0)
            continue
        end
        % Zero-forcing goes here
        %         x_est = inv(H)*y;
        x_exp = [tx_syms_mat_A(sc_ii,:); tx_syms_mat_B(sc_ii,:)];
        y_exp = H*x_exp;
        %         noise_p = abs(sum(y, 'all')-sum(y_exp, 'all')).^2;
        noise_p = sum(abs(sum(y - y_exp,1)).^2, 'all');
        snrs(sc_ii) = sum(sum(abs(y_exp), 1).^2)/noise_p;
    end
    % No water filling
    capacity_beamf(seg_ii) = Fs*log2(1+mean(snrs));
    snr_est_out(seg_ii) = mean(snrs)/4;
end

%% Alternate MAB + channel condition techniques
% These alternate estimates are included to determine the effect of channel
% estimation noise on overall performance - ie., how much SNR is degraded
% by channel estimation error, and how different the channel rank estimate
% is as a result of the same error

snr_est_alt_out = zeros(1, num_seg);
channel_condition_alt = zeros(1, num_seg);
for seg_ii=1:num_seg
    syms_f_mat_pc_A = payload_A_corr(:,:,seg_ii);
    syms_f_mat_pc_B = payload_B_corr(:,:,seg_ii);
    if isnan(start_idx(seg_ii))
        continue
    end
    channel_condition_mat = zeros(1,N_SC);
    for sc_ii = 1:length(SC_IND_DATA)
        sc_idx = SC_IND_DATA(sc_ii);
        y_rx = [syms_f_mat_pc_A(sc_idx,:) ; syms_f_mat_pc_B(sc_idx,:)];
        x_exp = [tx_syms_mat_A(sc_ii,:); tx_syms_mat_B(sc_ii,:)];
        % I think this line is wrong
        H_est = [y_rx(1,:)*x_exp(1,:)', y_rx(1,:)*x_exp(2,:)'; ...
                 y_rx(2,:)*x_exp(1,:)', y_rx(2,:)*x_exp(2,:)'];
        H_est = H_est / size(x_exp,2);
        y_exp = H_est*x_exp;
        noise_p = sum(abs(sum(y - y_exp,1)).^2, 'all');
        snrs(sc_ii) = sum(sum(abs(y_exp), 1).^2)/noise_p;
        channel_condition_mat(sc_ii) = 1/cond(H_est);
    end
    channel_condition_alt(seg_ii) = mean(channel_condition_mat);
    snr_est_alt_out(seg_ii) = mean(snrs);
end


%% Theoretical MIMO Calculations
% capacity_mimo_ideal = zeros(1,num_seg);
% capacity_mab_ideal = zeros(1,num_seg);
% for seg_ii=1:num_seg
%     H=squeeze(rx_H_est(:,:,:,seg_ii));
%
%     snr = max([snrs_rx_A(seg_ii) snrs_rx_B(seg_ii)]);
%
%     % MIMO capacity, equal rate for all streams (no waterfilling)
%     H_angle = angle(H);
%     % Probably not enough of a difference for it to matter that we average this
%     phase_delta = mean(H_angle(:,1,1)-H_angle(:,1,2)+H_angle(:,2,2)-H_angle(:,2,1));
%     % Another approximation
%     crosstalk = mean(abs(H(:,2,1))+abs(H(:,1,2)))/mean(abs(H(:,1,1))+abs(H(:,1,1)));
%     capacity_mimo_ideal(seg_ii) = Fs*log2((1+snr*(1+crosstalk^2))^2 - 2*(crosstalk*snr)^2*(1+cos(phase_delta)));
%
%     % Multi-array beamforming
%     capacity_mab_ideal(seg_ii) = Fs*log2(1+4*snr);
% end

%% Functions
function cap = capacity2(payloadA, payloadB, expectedA, expectedB, Fs)
payload = zeros(2, size(payloadA,1), size(payloadA,2));
payload(1,:,:) = payloadA;
payload(2,:,:) = payloadB;
expected = zeros(size(payload));
expected(1,:,:) = expectedA;
expected(2,:,:) = expectedB;
cap = beamform.estimate_total_capacity(payload, expected, Fs);
end
