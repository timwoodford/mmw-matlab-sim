function [ofdm_est, snr] = cef_est_ofdm(rx_cef, cef)
cef_len = length(cef);

% Method 1: Just use every segment
seg_len = 32;
ce_mat = zeros((cef_len-128)/seg_len, seg_len);
segments = (1:seg_len)+seg_len*(0:((cef_len-128)/seg_len - 1))';
for ii=1:size(segments,1)
    seg = segments(ii,:);
    cef_fft=fft(cef(seg));
    for jj=1:seg_len
        if abs(cef_fft(jj)) < 4
            cef_fft(jj) = NaN;
        end
    end
    ce_mat(ii,:) = fft(rx_cef(seg))./cef_fft;
end

ofdm_est = nanmean(ce_mat,1);
% plot(abs(ofdm_est));

% Method 2: Only use properly prefixed segments
% segments = [257:512; 1153:1408];


% Finally, SNR
snrs = zeros(1,size(segments,1));
figure(6);
for ii=1:size(segments,1)
    corr_seg = ifft(fft(rx_cef(segments(ii,:)))./ofdm_est);
    cef_seg=cef(segments(ii,:));
    ce_est = mean(corr_seg./cef_seg);
    corr_seg = corr_seg./ce_est;
    plot(cef_seg, 'o'); hold on; plot(corr_seg, '.');
    evm = abs(corr_seg - cef_seg).^2;
    aevm = mean(evm(:));
    snrs(ii) = 10*log10(1./aevm);
end
hold off;
snr=mean(snrs);
end