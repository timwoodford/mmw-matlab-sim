% WARP-derived code Copyright (c) 2015 Mango Communications - All Rights Reserved
% Distributed under the WARP License (http://warpproject.org/license)

% This version has been modified for maximum memory efficiency

LTS_CORR_THRESH               = 0.7; % Lower for low-SNR situations
FFT_OFFSET                    = 4;   % Number of CP samples to use in FFT (on average)
data = matfile('data/mimo/los/collect_mimo2_exhaustive_190722-160805.mat');
load(data.Properties.Source, '-regexp', '^(?!rx_\w)\w');
clear usrph

CACHE_SEG = 4096*16;

%% Decimate
if(INTERP_RATE == 1)
    %     raw_rx_dec_A = data.rx_data(1,:);
    %     raw_rx_dec_B = data.rx_data(2,:);
elseif(INTERP_RATE == 2)
    error('Use lofdm_mimo2_proc for handling decimation');
else
    error('Unknown decimation rate');
end

%% Split up segments
unit_len = size(transmit_A, 2) + CB_GAP;
mod_len = unit_len*length(txB_cbidx);
num_seg = length(txB_cbidx);
num_seg=4096*2;

%% Timesync & CFO Estimation
mimo_lts_A = zeros(192, num_seg, 'single');
mimo_lts_B = zeros(192, num_seg, 'single');
payload_A = zeros(N_SC, N_OFDM_SYMS, num_seg, 'single');
payload_B = zeros(N_SC, N_OFDM_SYMS, num_seg, 'single');
rx_H_est = zeros(N_SC, 2, 2, num_seg);
start_idx = zeros(1, num_seg);
est_noise = zeros(1, num_seg);
cfo_ests = zeros(1, num_seg);
% start_cache = [];
start_cache = 109;

profile on
for seg_ii=1:num_seg
    % This caching method prevents us from running the loop with parfor
    % Given how expensive data reading seems to be, this seems like an 
    % acceptable tradeoff
    % Matlab seems to do a lot of matrix operations in parallel anyways
    if mod(seg_ii, CACHE_SEG) == 1
        cache_rx_A = data.rx_data(1,(seg_ii-1)*unit_len + (1:unit_len*CACHE_SEG));
        cache_rx_B = data.rx_data(2,(seg_ii-1)*unit_len + (1:unit_len*CACHE_SEG));
        seg_ii
    end
    %     this_rx_A = data.rx_data(1,(seg_ii-1)*unit_len + (1:unit_len));
    %     this_rx_B = data.rx_data(2,(seg_ii-1)*unit_len + (1:unit_len));
    %     assert(all(this_rx_A == cache_rx_A(mod(seg_ii-1, CACHE_SEG)*unit_len + (1:unit_len))));
    %     assert(all(this_rx_B == cache_rx_B(mod(seg_ii-1, CACHE_SEG)*unit_len + (1:unit_len))));
    this_rx_A = cache_rx_A(mod(seg_ii-1, CACHE_SEG)*unit_len + (1:unit_len));
    this_rx_B = cache_rx_B(mod(seg_ii-1, CACHE_SEG)*unit_len + (1:unit_len));
    
    if isempty(start_cache)
%     if true
        % For simplicity, we'll only use RFA for LTS correlation and peak
        % discovery. A straightforward addition would be to repeat this process for
        % RFB and combine the results for detection diversity.
        
        % Complex cross correlation of Rx waveform with time-domain LTS
        lts_corr = abs(conv(conj(fliplr(lts_t)), this_rx_A));
        
        % Skip early and late samples - avoids occasional false positives from pre-AGC samples
        lts_corr = lts_corr(32:end-32);
        
        % Find all correlation peaks
        lts_peaks = find(lts_corr(1:1000) > LTS_CORR_THRESH*max(lts_corr));
        
        % Select best candidate correlation peak as LTS-payload boundary
        % In this MIMO example, we actually have 3 LTS symbols sent in a row.
        % The first two are sent by RFA on the TX node and the last one was sent
        % by RFB. We will actually look for the separation between the first and the
        % last for synchronizing our starting index.
        
        [LTS1, LTS2] = meshgrid(lts_peaks,lts_peaks);
        [lts_last_peak_index,y] = find(LTS2-LTS1 == length(lts_t));
        
        % Stop if no valid correlation peak was found
        if(isempty(lts_last_peak_index)) || lts_peaks(max(lts_last_peak_index)) < 160
            start_idx(seg_ii) = NaN;
            continue
        end
        
        % Set the sample indices of the payload symbols and preamble
        % The "+32" here corresponds to the 32-sample cyclic prefix on the preamble LTS
        % The "+192" corresponds to the length of the extra training symbols for MIMO channel estimation
        mimo_training_ind = lts_peaks(max(lts_last_peak_index)) + 32;
        payload_ind = mimo_training_ind + 192;
        
        % Subtract of 2 full LTS sequences and one cyclic prefixes
        % The "-160" corresponds to the length of the preamble LTS (2.5 copies of 64-sample LTS)
        lts_ind = mimo_training_ind-160;
        
        start_idx(seg_ii) = lts_ind;
        
        % Cache start index
        avail_start = start_idx(1:seg_ii);
        avail_start = avail_start(~isnan(avail_start));
        if sum(mode(avail_start)==avail_start) > 0.8*length(avail_start) && length(avail_start) > 200
            start_cache = mode(avail_start);
            fprintf('Caching start index %d at %d\n', start_cache, seg_ii);
        end
    else
        lts_ind = start_cache;
        mimo_training_ind = lts_ind + 160;
        payload_ind = mimo_training_ind + 192;
    end
    
    %Extract LTS (not yet CFO corrected)
    rx_lts = this_rx_A(lts_ind : lts_ind+159); %Extract the first two LTS for CFO
    rx_lts1 = rx_lts(-64+-FFT_OFFSET + (97:160));
    rx_lts2 = rx_lts(-FFT_OFFSET + (97:160));
    
    %Calculate coarse CFO est
    rx_cfo_est_lts = mean(unwrap(angle(rx_lts2 .* conj(rx_lts1))));
    rx_cfo_est_lts = rx_cfo_est_lts/(2*pi*64);
    cfo_ests(seg_ii) = rx_cfo_est_lts;
    
    % Apply CFO correction to raw Rx waveforms
    rx_cfo_corr_t = exp(-1i*2*pi*rx_cfo_est_lts*(0:length(this_rx_A)-1));
    rx_dec_cfo_corr_A = this_rx_A .* rx_cfo_corr_t;
    rx_dec_cfo_corr_B = this_rx_B .* rx_cfo_corr_t;
    
    % Estimate noise
    rx_lts = rx_dec_cfo_corr_A(lts_ind : lts_ind+159);
    rx_lts1 = rx_lts(-64+-FFT_OFFSET + [97:160]);
    rx_lts2 = rx_lts(-FFT_OFFSET + [97:160]);
    est_noise(seg_ii) = mean(abs(rx_lts1-rx_lts2).^2)/4;
    
    % Split up the remaining signal
    rx_lts_AA = mimo_lts_A((0:63) + 32 - FFT_OFFSET, seg_ii);
    rx_lts_BA = mimo_lts_A((0:63) + 32 + 64 + 32 - FFT_OFFSET, seg_ii);
    rx_lts_AB = mimo_lts_B((0:63) + 32 - FFT_OFFSET, seg_ii);
    rx_lts_BB = mimo_lts_B((0:63) + 32 + 64 + 32 - FFT_OFFSET, seg_ii);
    
    rx_lts_AA_f = fft(rx_lts_AA);
    rx_lts_BA_f = fft(rx_lts_BA);
    rx_lts_AB_f = fft(rx_lts_AB);
    rx_lts_BB_f = fft(rx_lts_BB);
    
    % Calculate channel estimate
    rx_H_est_AA = lts_f .* rx_lts_AA_f.';
    rx_H_est_BA = lts_f .* rx_lts_BA_f.';
    
    rx_H_est_AB = lts_f .* rx_lts_AB_f.';
    rx_H_est_BB = lts_f .* rx_lts_BB_f.';
    
    % Store in matrix
    rx_H_est(:,1,1,seg_ii) = rx_H_est_AA.';
    rx_H_est(:,2,1,seg_ii) = rx_H_est_BA.';
    rx_H_est(:,1,2,seg_ii) = rx_H_est_AB.';
    rx_H_est(:,2,2,seg_ii) = rx_H_est_BB.';
    
    payload_vec_A = rx_dec_cfo_corr_A(payload_ind : payload_ind+N_OFDM_SYMS*(N_SC+CP_LEN)-1);
    payload_mat_A = reshape(payload_vec_A, (N_SC+CP_LEN), N_OFDM_SYMS);
    payload_vec_B = rx_dec_cfo_corr_B(payload_ind : payload_ind+N_OFDM_SYMS*(N_SC+CP_LEN)-1);
    payload_mat_B = reshape(payload_vec_B, (N_SC+CP_LEN), N_OFDM_SYMS);
    
    % Remove the cyclic prefix, keeping FFT_OFFSET samples of CP (on average)
    payload_A(:,:,seg_ii) = fft(payload_mat_A(CP_LEN-FFT_OFFSET+(1:N_SC), :), N_SC, 1);
    payload_B(:,:,seg_ii) = fft(payload_mat_B(CP_LEN-FFT_OFFSET+(1:N_SC), :), N_SC, 1);
end
profile viewer
profile off

% Memory saving
clear cache_rx_A
clear cache_rx_B

clear this_rx_A this_rx_B rx_cfo_est_lts rx_cfo_corr_t
clear lts_corr lts_peaks LTS1 LTS2 rx_lts rx_lts1 rx_lts2
clear payload_vec_A payload_mat_A payload_vec_B payload_mat_B
clear rx_H_est_AA rx_H_est_AB rx_H_est_BA rx_H_est_BB
clear rx_lts_AA_f rx_lts_BA_f rx_lts_AB_f rx_lts_BB_f
clear rx_lts_AA rx_lts_AB rx_lts_BA rx_lts_BB

%% Pilot-based correction
pilot_phase_errors = zeros(N_OFDM_SYMS, num_seg);
payload_A_corr = zeros(N_SC, N_OFDM_SYMS, num_seg);
payload_B_corr = zeros(N_SC, N_OFDM_SYMS, num_seg);

for seg_ii=1:num_seg
    rx_H_est_AA = rx_H_est(:,1,1,seg_ii);
    
    % Equalize pilots
    % Because we only used Tx RFA to send pilots, we can do SISO equalization
    % here. This is zero-forcing (just divide by chan estimates)
    syms_eq_mat_pilots = payload_A(:,:,seg_ii) ./ repmat(rx_H_est_AA, 1, N_OFDM_SYMS);
    
    % SFO manifests as a frequency-dependent phase whose slope increases
    % over time as the Tx and Rx sample streams drift apart from one
    % another. To correct for this effect, we calculate this phase slope at
    % each OFDM symbol using the pilot tones and use this slope to
    % interpolate a phase correction for each data-bearing subcarrier.
    
    % Extract the pilot tones and "equalize" them by their nominal Tx values
    pilots_f_mat = syms_eq_mat_pilots(SC_IND_PILOTS,:);
    pilots_f_mat_comp = pilots_f_mat.*pilots_mat_A;
    
    % Calculate the phases of every Rx pilot tone
    pilot_phases = unwrap(angle(fftshift(pilots_f_mat_comp, 1)), [], 1);
    
    pilot_spacing_mat = repmat(mod(diff(fftshift(SC_IND_PILOTS)), 64).', 1, N_OFDM_SYMS);
    pilot_slope_mat = mean(diff(pilot_phases) ./ pilot_spacing_mat);
    
    % Calculate the SFO correction phases for each OFDM symbol
    pilot_phase_sfo_corr = fftshift((-32:31).' * pilot_slope_mat, 1);
    pilot_phase_corr = exp(-1i*(pilot_phase_sfo_corr));
    
    % Apply the pilot phase correction per symbol
    syms_f_mat_A = payload_A(:,:,seg_ii) .* pilot_phase_corr;
    syms_f_mat_B = payload_B(:,:,seg_ii) .* pilot_phase_corr;
    
    % Extract the pilots and calculate per-symbol phase error
    pilots_f_mat = syms_eq_mat_pilots(SC_IND_PILOTS, :);
    pilot_phase_err = angle(mean(pilots_f_mat.*pilots_mat_A));
    pilot_phase_corr = repmat(exp(-1i*pilot_phase_err), N_SC, 1);
    
    % Save the symbol-wide phase error for later analysis.
    % This is potentially interesting to us because I suspect that there is
    % substantial random phase offset over time between different clocks,
    % beyond just residual CFO
    pilot_phase_errors(:,seg_ii) = pilot_phase_err;
    
    % Apply pilot phase correction to both received streams
    payload_A_corr(:,:,seg_ii) = syms_f_mat_A .* pilot_phase_corr;
    payload_B_corr(:,:,seg_ii) = syms_f_mat_B .* pilot_phase_corr;
end

clear pilots_f_mat pilots_f_mat_comp pilot_phases pilot_spacing_mat
clear pilot_slope_mat pilot_phase_sfo_corr pilot_phase_corr
clear syms_f_mat_A syms_f_mat_B pilots_f_mat pilot_phase_err
clear pilot_phase_corr pilot_phase_errors

%% SNR Estimation
% Use OFDMA separation to estimate SNR of each possible stream
stream_snrs = zeros(num_seg, 2, 2);
for seg_ii=1:num_seg
    % Retrieve H
    rx_H_est_AA = rx_H_est(:,1,1,seg_ii);
    rx_H_est_BA = rx_H_est(:,1,2,seg_ii);
    rx_H_est_AB = rx_H_est(:,2,1,seg_ii);
    rx_H_est_BB = rx_H_est(:,2,2,seg_ii);
    % Get symbols
    syms_f_mat_pc_A = payload_A_corr(:,:,seg_ii);
    syms_f_mat_pc_B = payload_B_corr(:,:,seg_ii);
    % Grab the correct original payload
    tx_syms_mat_A = reshape(tx_syms_A(txA_cbidx(seg_ii),:), length(SC_IND_DATA)/2, N_OFDM_SYMS);
    tx_syms_mat_B = reshape(tx_syms_B(txB_cbidx(seg_ii),:), length(SC_IND_DATA)/2, N_OFDM_SYMS);
    % Calculate SNRs
    stream_snrs(seg_ii,1,1) = rfrx.ofdm_snr_est(syms_f_mat_pc_A, tx_syms_mat_A, rx_H_est_AA, SC_IND_DATA(1:2:end));
    stream_snrs(seg_ii,1,2) = rfrx.ofdm_snr_est(syms_f_mat_pc_B, tx_syms_mat_A, rx_H_est_AB, SC_IND_DATA(1:2:end));
    stream_snrs(seg_ii,2,2) = rfrx.ofdm_snr_est(syms_f_mat_pc_B, tx_syms_mat_B, rx_H_est_BB, SC_IND_DATA(2:2:end));
    stream_snrs(seg_ii,2,1) = rfrx.ofdm_snr_est(syms_f_mat_pc_A, tx_syms_mat_B, rx_H_est_BA, SC_IND_DATA(2:2:end));
end

figure(3); clf
subplot(2,1,1); plot(squeeze(stream_snrs(:,1,1)))
hold on; plot(squeeze(stream_snrs(:,1,2)))
legend({'1 -> 1', '1 -> 2'});
title('SNR Values');
subplot(2,1,2); plot(squeeze(stream_snrs(:,2,1)))
hold on; plot(squeeze(stream_snrs(:,2,2)))
legend({'2 -> 1', '2 -> 2'});

%% RSSI

rssi = squeeze(mean(abs(rx_H_est(SC_IND_DATA,:,:,:)),1));

figure(4); clf
subplot(2,1,1); plot(squeeze(rssi(1,1,:)))
hold on; plot(squeeze(rssi(1,2,:)))
legend({'1 -> 1', '1 -> 2'});
title('RSS Values');
subplot(2,1,2); plot(squeeze(rssi(2,1,:)))
hold on; plot(squeeze(rssi(2,2,:)))
legend({'2 -> 1', '2 -> 2'});


%% MIMO
% This is where things get interesting for us
% We have 3 main options here, each with different capacity characteristics
% * Spatial multiplexing (ZF, MMSE)
% * Open-loop MIMO - ratio combining at rx
% * Closed-loop MIMO - ratio combining at tx and rx

% Spatial multiplexing (ZF, MMSE)
capacity_multiplexing_zf = zeros(1, num_seg);
channel_condition = zeros(1, num_seg);
for seg_ii=1:num_seg
    % Retrieve H
    rx_H_est_AA = rx_H_est(:,1,1,seg_ii);
    rx_H_est_BA = rx_H_est(:,1,2,seg_ii);
    rx_H_est_AB = rx_H_est(:,2,1,seg_ii);
    rx_H_est_BB = rx_H_est(:,2,2,seg_ii);
    syms_f_mat_pc_A = payload_A_corr(:,:,seg_ii);
    syms_f_mat_pc_B = payload_B_corr(:,:,seg_ii);
    syms_eq_mat_A = zeros(N_SC, N_OFDM_SYMS);
    syms_eq_mat_B = zeros(N_SC, N_OFDM_SYMS);
    channel_condition_mat = zeros(1,N_SC);
    for sc_idx = [SC_IND_DATA, SC_IND_PILOTS]
        y = [syms_f_mat_pc_A(sc_idx,:) ; syms_f_mat_pc_B(sc_idx,:)];
        H = [rx_H_est_AA(sc_idx), rx_H_est_BA(sc_idx) ; rx_H_est_AB(sc_idx), rx_H_est_BB(sc_idx)];
        % Zero-forcing goes here
        x = inv(H)*y;
        syms_eq_mat_A(sc_idx, :) = x(1,:);
        syms_eq_mat_B(sc_idx, :) = x(2,:);
        channel_condition_mat(sc_idx) = rcond(H);
    end
    channel_condition(seg_ii) = mean(channel_condition_mat);
    % Grab the correct original payload
    tx_syms_mat_A = reshape(tx_syms_A(txA_cbidx(seg_ii),:), length(SC_IND_DATA)/2, N_OFDM_SYMS);
    tx_syms_mat_B = reshape(tx_syms_B(txB_cbidx(seg_ii),:), length(SC_IND_DATA)/2, N_OFDM_SYMS);
    % Chop out the data channels.  We're doing OFDMA on top of spatial
    % multiplexing here, so we'll manually cause interference for this case
    rx_syms_mat_A = syms_eq_mat_A(SC_IND_DATA(1:2:end), :);
    rx_syms_mat_B = syms_eq_mat_B(SC_IND_DATA(2:2:end), :);
    % Do EVM-based capacity estimation
    capacity_multiplexing_zf(seg_ii) = ...
        capacity2(rx_syms_mat_A, rx_syms_mat_B, tx_syms_mat_A, tx_syms_mat_B, Fs);
end


%% Spatial multiplexing (MMSE)
capacity_multiplexing_mmse = zeros(1, num_seg);
for seg_ii=1:num_seg
    syms_f_mat_pc_A = payload_A_corr(:,:,seg_ii);
    syms_f_mat_pc_B = payload_B_corr(:,:,seg_ii);
    syms_eq_mat_A = zeros(N_SC, N_OFDM_SYMS);
    syms_eq_mat_B = zeros(N_SC, N_OFDM_SYMS);
    for sc_idx = [SC_IND_DATA, SC_IND_PILOTS]
        y = [syms_f_mat_pc_A(sc_idx,:) ; syms_f_mat_pc_B(sc_idx,:)];
        H = [rx_H_est_AA(sc_idx), rx_H_est_BA(sc_idx) ; rx_H_est_AB(sc_idx), rx_H_est_BB(sc_idx)];
        noise = est_noise(seg_ii);
        % MMSE goes here - only difference from ZF is use of noise terms
        x = inv(H'*H+noise*eye(2))*H'*y;
        syms_eq_mat_A(sc_idx, :) = x(1,:);
        syms_eq_mat_B(sc_idx, :) = x(2,:);
    end
    % Grab the correct original payload
    tx_syms_mat_A = reshape(tx_syms_A(txA_cbidx(seg_ii),:), length(SC_IND_DATA)/2, N_OFDM_SYMS);
    tx_syms_mat_B = reshape(tx_syms_B(txB_cbidx(seg_ii),:), length(SC_IND_DATA)/2, N_OFDM_SYMS);
     % Chop out the data channels.  We're doing OFDMA on top of spatial
    % multiplexing here, so we'll manually cause interference for this case
    rx_syms_mat_A = syms_eq_mat_A(SC_IND_DATA(1:2:end), :);
    rx_syms_mat_B = syms_eq_mat_B(SC_IND_DATA(2:2:end), :);
    % Do EVM-based capacity estimation
    capacity_multiplexing_mmse(seg_ii) = ...
        capacity2(rx_syms_mat_A, rx_syms_mat_B, tx_syms_mat_A, tx_syms_mat_B, Fs);
end

% Open-loop MIMO - maybe not be possible with current signal

% Closed-loop MIMO (MRC)

%% Theoretical MIMO Calculations
for seg_ii=1:num_seg
    H=zeros(2,2,64);
    H(1,1,:) = rx_H_est_AA;
    H(1,2,:) = rx_H_est_AB;
    H(2,1,:) = rx_H_est_BA;
    H(2,2,:) = rx_H_est_BB;
    
    snr = 5; % ???? need to figure out a good way to calculate this
    
    % MIMO capacity, equal rate for all streams (no waterfilling)
    H_angle = angle(H);
    % Probably not enough of a difference for it to matter that we average this
    phase_delta = mean(H_angle(1,1,:)-H_angle(1,2,:)+H_angle(2,2,:)-H_angle(2,1,:));
    % Another approximation
    crosstalk = mean((abs(H(2,1,:))+abs(H(1,2,:)))/2);
    capacity_mimo_ideal(seg_ii) = Fs*log2((1+snr*(1+crosstalk^2))^2 - 2*(crosstalk*snr)^2*(1+cos(phase_delta)));
    
    % Multi-array beamforming
    capacity_mab_ideal = Fs*log2(1+4*snr);
end
%%
capacity2(rx_syms_mat_A, rx_syms_mat_B, tx_syms_mat_A, tx_syms_mat_B, Fs)

%% Functions
function cap = capacity2(payloadA, payloadB, expectedA, expectedB, Fs)
payload = zeros(2, size(payloadA,1), size(payloadA,2));
payload(1,:,:) = payloadA;
payload(2,:,:) = payloadB;
expected = zeros(size(payload));
expected(1,:,:) = expectedA;
expected(2,:,:) = expectedB;
cap = beamform.estimate_total_capacity(payload, expected, Fs);
end
