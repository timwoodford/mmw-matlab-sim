#!/bin/sh
fnames=`find ../openmili_stream/rtl/ -iname '*.vhd'`

for fn in $fnames; do
    fnbase=`basename $fn`
    src=`find generated_hdl/ -iname "$fnbase"|head -n1`
    cp $src $fn
done
