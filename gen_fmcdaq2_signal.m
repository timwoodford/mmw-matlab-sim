stf=train.getstf_80211ad().';
cef=train.cef_80211ay(2,1);
scaling_factor=10000; % Scaling factor we used previously for sine wave
zero_pad=256;

% The signal will be zero padding, then 1 STF sequence followed by 64 CEFs
cmplx_sig = scaling_factor*[zeros(1,zero_pad) stf repmat(cef, 1, 64)];

% Interleaved complex format (alternating real, imag - same as C++
% std::complex
interleaved = reshape([real(cmplx_sig); imag(cmplx_sig)], 2*numel(cmplx_sig), 1);

fh2=fopen('training_sig.dat','w');
fwrite(fh2, scaled_data, 'int16');
fclose(fh2);